## Donut Business International ERP Client
ERP like Application, build with Next.js with 7 main modules:

- Finance
- Inventory
- Personal (HR)
- Production
- Purchasing
- Sales

## Run The Application

1. Install node modules:
    
    npm install
    
2. Run the development server:
    
    npm run dev
    
3. Open [http://127.0.0.1:3000](http://127.0.0.1:3000) with your browser.
4. Run the [Backend Server](https://gitlab.com/sugar-daddy/dbi-api).
