import Image from "next/image";
import { useEffect, useState } from "react";

export default function Pagination({data, itemsPerPage, setPagedData}) {
    const [currentPage, setCurrentPage] = useState(1)
    const [itemOffset, setItemOffset] = useState(0)
    const [maxPage, setMaxPage] = useState(1)
    
    useEffect(()=>{
        let newOffset = itemOffset
        while(newOffset >= data.length)
            newOffset -= itemsPerPage
        const endOffset = newOffset + itemsPerPage
        setItemOffset(newOffset)
        setPagedData(data.slice(newOffset, endOffset))
        setCurrentPage(Math.floor(newOffset / itemsPerPage) + 1)
        setMaxPage(Math.ceil(data.length / itemsPerPage))
    }, [data, itemOffset, itemsPerPage])

    const handlePageClick = (direction) => {
        const newOffset = itemOffset + direction * itemsPerPage
        if (newOffset >= 0 && newOffset < data.length)
            setItemOffset(newOffset)
    }

    return (
        <div className="float-right text-lg space-x-2 items-center flex">
            <button className="h-[30px]" name="backPage" onClick={()=>handlePageClick(-1)}><Image src="/arrow-left.png" alt="" width={30} height={30}/></button>
            <span className="px-1.5 text-sm  border-2 rounded-md bg-gray-100">{currentPage}/{maxPage}</span>
            <button className="h-[30px]" name="nextPage" onClick={()=>handlePageClick(1)}><Image src="/arrow-right.png" alt="" width={30} height={30}/></button>
        </div>
    )
}