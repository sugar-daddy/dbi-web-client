export default function ManageMaterialsForm({data}) {
    const [newData, setNewData] = useState(()=>
      data ?
      data :
      {
        id: 0,
        type: '',
        name: '',
        amount: 0,
      }
    )
    return (
      <form>
        <h1>{data ? 'Edit' : 'Add'} Manage Materials</h1>
        <label>Type <input type="text" value={newData.type} /></label>
        <label>Name <input type="text" /></label>
        <label>Amount <input type="number" /></label>
        <input type="submit" value="POST"/>
      </form>
    )
  }