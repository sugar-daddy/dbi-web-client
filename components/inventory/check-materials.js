import Image from "next/image";
import { useContext, useEffect, useState } from "react";
import TableHeader from "../tableHeader";
import Pagination from "../pagination";
import FilterGroup from "/components/filterGroup"
import { getMaterial, putMaterial } from "../../middleware/material";
import { StatusMessageContext } from "../../stores/status-message-context";
import { checkResponseStatusOK } from "../utilityFunc";
import PopUp from "../popUp";
import CheckMaterialForm from "../../components/inventory/checkMaterialForm"

export default function CheckMaterials({setRequestMaterials, setEditData}) {
    const [filter, setFilter] = useState('all')
    const [pagedData, setPagedData] = useState(null)
    const [showAddPopUp, setShowAddPopUp] = useState(false)
    const statusMessage = useContext(StatusMessageContext)
    const parameter = () => filter == 'all' ? {} : {'type': filter}
    const [manageMaterialsData, setManageMaterialsData]= useState(null)
    const [chosenData, setChosenData] = useState({
      id: 0,
      type: "",
      name: "",
      quantity: 0,
      measurement: "",
    })

    const handleGetData = async() => {
      const [status,dataMaterialManage] = await getMaterial(parameter())
      if (checkResponseStatusOK(status))
      setManageMaterialsData(dataMaterialManage)
      else
        statusMessage.showMessage(status, 'Fetch Data!')
    }

    const handleChange = e => {
      const {name, value} = e.target
      if (chosenData?.id)
        setChosenData(data => ({
          ...data,
          [name]: value
        }))
      else
        setChosenData(data => ({
          ...data,
          date: new Date().toISOString().slice(0, 10),
          [name]: value
        }))
    }

    const handleSubmit= async e => {
      e.preventDefault()

        //update data
        const status = await putMaterial(chosenData)
        const text = `Update on Material with ID ${chosenData.id}!`
        statusMessage.showMessage(status, text)
      
      setShowAddPopUp(false) // close pop up
      // get cash flow data
      await handleGetData()
    }

    useEffect(async() =>{
      await handleGetData()
    },[filter]) 

    return (
        <><div className="p-5 space-y-2 w-full">
        <div className="flex justify-between relative">
          <div className="w-2/5">
            <h2>Check Materials</h2>
            <FilterGroup filters={['all', 'raw', 'semi-finished', 'finished']} activeFilter={filter} setActiveFilter={setFilter} />
          </div>
          <button onClick={() => setRequestMaterials(true)} className="bg-gray-200 hover:bg-gray-300 border-2 border-gray-500 rounded-md py-0.5 px-1 h-fit absolute bottom-0 right-0 flex items-center">Materials Requests</button>
        </div>
        {manageMaterialsData &&
          <>
            <TableHeader headingNames={['Id', 'Type', 'Name', 'Quantity', 'Measurement', 'Action']}>
              {pagedData && pagedData.map((data) => <tr key={data.id} className="border-b-2 divide-x-2 w-full h-full">
                <td className="px-2 py-1 text-center">{data.id}</td>
                <td className="px-2 py-1 capitalize">{data.type}</td>
                <td className="px-2 py-1 text-center capitalize">{data.name}</td>
                <td className="px-2 py-1 text-right">{data.quantity}</td>
                <td className="px-2 py-1 text-right">{data.measurement}</td>
                <td className="px-2 py-1 flex justify-center h-full">
                  <button onClick={() => { setShowAddPopUp(!showAddPopUp); setChosenData(data); } }><Image src="/pencil.svg" alt="" width={20} height={20} /></button>
                </td>
              </tr>
              )}
            </TableHeader>
          <Pagination data={manageMaterialsData} setPagedData={setPagedData} itemsPerPage={5}/>
          </>
        }
      </div>
      <PopUp showPopUp={showAddPopUp} setShowPopUp={setShowAddPopUp} title={`${chosenData?.id ? 'Edit' : 'Add'} Material`}>
          <CheckMaterialForm
            chosenData={chosenData}
            handleChange={handleChange}
            handleSubmit={handleSubmit} />
      </PopUp>
      </>
    )
}