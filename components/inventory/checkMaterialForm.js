export default function CheckMaterialForm({chosenData, handleSubmit, handleChange}) {

    return (
      <div className="w-full flex justify-center items-center">
        <form onSubmit={handleSubmit} className="w-full grid grid-cols-1 grid-rows-4 space-y-3">
          <div className="w-full justify-between flex items-center">
            <label className="w-1/3">Type </label>
            <select className="w-2/3" value={chosenData && chosenData.type || ''} name="type" onChange={handleChange} required>
              <option disabled></option>
              <option value="raw">Raw</option>
              <option value="semi-finished">Semi-Finished</option>
              <option value="finished">Finished</option>
            </select>
          </div>
          <div className="w-full justify-between flex items-center">
            <label className="w-1/3">Name</label>
            <input className="w-2/3" type="text" name="name" onChange={handleChange} value={chosenData && chosenData.name || ''} required />
          </div>
          <div className="w-full justify-between flex items-center">
            <label className="w-1/3">Quantity</label>
            <input className="w-2/3" type="number" name="quantity" onChange={handleChange} value={chosenData && chosenData.quantity || ''} required />
          </div>
          <div className="w-full justify-between flex items-center">
            <label className="w-1/3">Measurement</label>
            <input className="w-2/3" type="text" name="measurement" onChange={handleChange} value={chosenData && chosenData.measurement || ''} required />
          </div>
          <div className="w-full items-center">
            <button className="px-2 py-1 border-2 border-black rounded-md hover:bg-gray-200" type="submit">POST</button>
          </div>
        </form>
      </div>
    )
  }