import Image from "next/image";
import { useContext, useEffect, useState } from "react";
import { getRequestMaterial, putRequestMaterial } from "../../middleware/request-material";
import { StatusMessageContext } from "../../stores/status-message-context";
import { UserContext } from "../../stores/user-context";
import Pagination from "../pagination";
import { checkResponseStatusOK, formatDatetimeToDate } from "../utilityFunc";

export default function RequestMaterials({setIsNewEdit, setRequestMaterials}) {
    const [pagedData, setPagedData] = useState(null)
    const statusMessage = useContext(StatusMessageContext)
    const userContext = useContext(UserContext)

    const [requestMaterialsData, setRequestMaterialsData]= useState(null)
    const handleGetData = async() => {
      const [status,dataMaterialRequest] = await getRequestMaterial()
      if (checkResponseStatusOK(status))
        setRequestMaterialsData(dataMaterialRequest)
      else
        statusMessage.showMessage(status, 'Fetch Data!')
    }

    useEffect(async()=> {
      await handleGetData()
    },[])

    const handlerAction = async(idrequest, isAcceptIt, idx) => {
      const data = {id: idrequest, status: isAcceptIt, employee_id: userContext.user.id}
      console.log(data)
      const status = await putRequestMaterial(data)
      const text = `Update on Request Material with ID ${idrequest}!`
      statusMessage.showMessage(status, text)
      await handleGetData()
    }

    return (
        <div className="p-5 space-y-2 w-full">
        <div className="flex justify-between relative">
          <div>
            <h2>Request Materials</h2>
          </div>
          <button onClick={()=>setRequestMaterials(false)} className="bg-gray-200 hover:bg-gray-300 border-2 border-gray-500 rounded-md py-0.5 px-1 h-fit absolute bottom-0 right-0 flex items-center">Check Materials</button>
        </div>
        {requestMaterialsData &&
          <> 
            <table className="min-w-full border-2 text-center">
              <thead className="border-b-2">
                <tr>
                  <th colSpan={1} rowSpan={2} className="text-sm font-medium text-gray-900 px-6 py-4 border-r-2">
                    Request Date
                  </th>
                  <th colSpan={1} rowSpan={2} className="text-sm font-medium text-gray-900 px-6 py-4 border-r-2">
                    Request ID
                  </th>
                  <th colSpan={4} rowSpan={1} className="text-sm font-medium text-gray-900 px-6 py-4 border-r-2">
                    Materials
                  </th>
                  <th colSpan={1} rowSpan={2} className="text-sm font-medium text-gray-900 px-6 py-4 border-r-2">
                    Request For
                  </th>
                  <th colSpan={1} rowSpan={2} className="text-sm font-medium text-gray-900 px-6 py-4 border-r-2">
                    Action
                  </th>
                </tr>
                <tr>
                  <th colSpan={1} rowSpan={1} className="text-sm font-medium text-gray-900 px-6 py-4 border-r-2 border-t-2">
                    ID
                  </th>
                  <th colSpan={1} rowSpan={1} className="text-sm font-medium text-gray-900 px-6 py-4 border-r-2 border-t-2">
                    Type
                  </th>
                  <th colSpan={1} rowSpan={1} className="text-sm font-medium text-gray-900 px-6 py-4 border-r-2 border-t-2">
                    Name
                  </th>
                  <th colSpan={1} rowSpan={1} className="text-sm font-medium text-gray-900 px-6 py-4 border-r-2 border-t-2">
                    Amount
                  </th>
                </tr>
              </thead>
              <tbody>
              { pagedData &&
                pagedData.map((data, index) =>
                data.listdetailrequest.map((detailData, idx) =>
                <tr key={''+data.id + detailData.id} className="h-fit">
                  {idx == 0 && 
                    <>
                  <td colSpan={1} rowSpan={data.listdetailrequest.length} className="text-sm font-medium text-gray-900 px-6 py-4 border-r-2 border-t-2">
                    {formatDatetimeToDate(data.date)}
                  </td>
                  <td colSpan={1} rowSpan={data.listdetailrequest.length} className="text-sm font-medium text-gray-900 px-6 py-4 border-r-2 border-t-2">
                    {data.id}
                  </td>
                    </>
                  }
                    <td className="text-sm font-medium text-gray-900 px-6 py-4 border-r-2 border-t-2">
                      {detailData.id}
                    </td>
                    <td className="text-sm font-medium text-gray-900 px-6 py-4 border-r-2 border-t-2 capitalize">
                      {detailData.material.type}
                    </td>
                    <td className="text-sm font-medium text-gray-900 px-6 py-4 border-r-2 border-t-2 capitalize">
                      {detailData.material.name}
                    </td>
                    <td className="text-sm font-medium text-gray-900 px-6 py-4 border-r-2 border-t-2">
                      {detailData.amount}
                    </td>

                  {idx == 0 && 
                    <>
                  <td colSpan={1} rowSpan={data.listdetailrequest.length} className="text-sm font-medium text-gray-900 px-6 py-4 border-r-2 border-t-2 capitalize">
                    {data.sales_id 
                      ? "sales"
                      : "production"
                    }
                  </td>
                  <td colSpan={1} rowSpan={data.listdetailrequest.length} className="text-sm font-medium text-gray-900 px-6 py-4 border-r-2 border-t-2 space-x-5">
                    {data.isaccepted == 0
                      ?
                      <>
                    <button className="hover:bg-rose-500 px-2 py-1 pt-2 rounded-md" onClick={()=>handlerAction(data.id, 2, index)}> <Image src="/cross.svg"  alt="" width={20} height={20}/> </button>
                    <button className="hover:bg-[#83BD75] px-2 py-1 pt-2 rounded-md" onClick={()=>handlerAction(data.id, 1, index)}><Image src="/check.svg"  alt="" width={20} height={20}/></button>
                      </>
                      : data.isaccepted == 1
                      ? <Image src="/check done.svg" alt="" width={20} height={20}/>
                      
                      : <Image src="/cross denied.svg" alt="" width={20} height={20}/>
                    }
                  </td>
                    </>
                  }
                </tr>
                )
              )}</tbody>
            </table>
            <Pagination data={requestMaterialsData} setPagedData={setPagedData} itemsPerPage={2}/>
          </>
        }
      </div>
    )
}