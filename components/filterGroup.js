export default function FilterGroup({filters, activeFilter, setActiveFilter}) {
  return (
    <>
      {filters.map((filter) =>
        <button key={filter} onClick={()=>setActiveFilter(filter)} className={`border-2 border-gray-500 capitalize text-center px-2 py-1 ${(activeFilter != filter ? "bg-gray-200 hover:bg-gray-300" : undefined)}`}>{filter}</button>
      )}
    </>
  )
}