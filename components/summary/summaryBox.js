function SummaryBox({title, boldText, grayText}) {
  return (
    <div className="py-2 pr-2 pl-4 bg-gray-200 w-56">
      <h3>{title}</h3>
      {grayText == '$'
        ? <h2 className="font-bold"><span className="text-gray-400 font-semibold">{grayText}</span> {boldText}</h2>
        : <h2 className="font-bold capitalize">{boldText} <span className="text-gray-400 font-semibold">{grayText}</span></h2>
      }
    </div>
  )
}

export default SummaryBox