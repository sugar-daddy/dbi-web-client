export default function CashFlowForm({chosenData, handleSubmit, handleChange}) {

  return (
    <div className="w-full flex justify-center items-center">
      <form onSubmit={handleSubmit} className="w-full grid grid-cols-1 grid-rows-4 space-y-3">
        <div className="w-full justify-between flex items-center">
          <label className="w-1/3">Status </label>
          <select className="w-2/3" value={chosenData && chosenData.status || ''} name="status" onChange={handleChange} required>
            <option disabled></option>
            <option value="credit">Credit</option>
            <option value="debit">Debit</option>
          </select>
        </div>
        <div className="w-full justify-between flex items-center">
          <label className="w-1/3">Description</label>
          <input className="w-2/3" type="text" name="descriptions" onChange={handleChange} value={chosenData && chosenData.descriptions || ''} required />
        </div>
        <div className="w-full justify-between flex items-center">
          <label className="w-1/3">Amount</label>
          <input className="w-2/3" type="number" name="amount" onChange={handleChange} value={chosenData && chosenData.amount || ''} required />
        </div>
        <div className="w-full items-center">
          <button className="px-2 py-1 border-2 border-black rounded-md hover:bg-gray-200" type="submit">POST</button>
        </div>
      </form>
    </div>
  )
}