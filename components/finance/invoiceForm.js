export default function AddInvoiceForm({data, handleSubmit}) {

  return (
    <div className="w-full flex justify-center items-center">
      <form onSubmit={handleSubmit} className="w-full grid grid-cols-1 grid-rows-4 space-y-3">
        <div className="w-full flex items-center">
          <label className="w-1/4"><div className="px-3 py-2 w-fit rounded-md bg-gray-300">{data.ref}</div></label>
          <div className="w-3/4 space-x-2">
            <input className="w-[64.35%] bg-gray-300" type="text" value={data.id} name="refId" readOnly />
            <input className="w-1/3 bg-gray-300" type="text" name="refDate" value={data.date} readOnly />
          </div>
        </div>
        <div className="w-full flex items-center">
          <label className="w-1/4">{data.ref=='PO'?'Vendor':'Customer'}</label>
          <input className="w-3/4 bg-gray-300" type="text" name="external" value={data.external.name} readOnly />
        </div>
        <div className="w-full flex items-center">
          <label className="w-1/4">Amount</label>
          <input className="w-3/4 bg-gray-300" type="number" name="amount" value={data.amount} readOnly />
        </div>
        <div className="w-full items-center">
          <button className="px-2 py-1 border-2 border-black rounded-md hover:bg-gray-200" type="submit">POST INVOICE</button>
        </div>
      </form>
    </div>
  )
}