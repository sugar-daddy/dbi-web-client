import Link from 'next/link';

export default function DropdownButtonNav({moduleName, moduleParts, moduleShow, setModuleShow, urlPath, disabled}) {
  const urlPathSection = Array.from(urlPath)

  return (
    <>
      <button onClick={() => setModuleShow(String(moduleName))} disabled={disabled} className={""+(urlPathSection[1]==String(moduleName) && "bg-red-300 text-white")+' '+(disabled ? 'bg-gray-500': 'hover:bg-red-300 hover:text-white cursor-pointer')+" px-4 py-2 flex items-center text-sm font-medium capitalize"} type="button">{moduleName} <svg className="ml-2 w-4 h-4" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M19 9l-7 7-7-7"></path></svg></button>
      {String(moduleShow) == String(moduleName) && Array.from(moduleParts).map(modulePart => 
        <Link href={`/${moduleName}/${modulePart.page}`} key={modulePart.page}>
          <a className={"px-7 py-2 w-full cursor-pointer text-sm font-medium text-white hover:bg-red-300 "+(urlPathSection[2] == String(modulePart.page) ? 'bg-red-300':'bg-red-600')}>{modulePart.uiName}</a>
        </Link>
      )}
    </>
  )
}