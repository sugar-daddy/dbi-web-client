import { useRouter } from 'next/router';
import { useContext, useEffect } from "react";
import { fetchCookie, fetchLogout } from '../middleware/authentication';
import { StatusMessageContext } from '../stores/status-message-context';
import { UserContext } from "../stores/user-context";
import Header from "./header";
import Navbar from "./navbar";
import StatusMessage from './statusMessage';
import { checkResponseStatusOK } from './utilityFunc';

export default function MainLayout({isOpen, setIsOpen, children}) {
  const router = useRouter()
  const userContext = useContext(UserContext)
  
  const handleLogout = async () => {
    // logout
    const status = await fetchLogout()
    if (status == 200) {
      userContext.setUser(null)
      router.push('/login')
    }
  }
  
  useEffect(async () => {
    // get cookie from server
    const [status, data] = await fetchCookie()
    if (checkResponseStatusOK(status))  //if cookie exist, set user
      userContext.setUser(data)
    else  // if no cookie, then login
      return router.replace('/login')
  }, [])

  return (
    userContext.user &&
    <>
      <div className="flex items-center h-20 px-5 md:px-10 w-full border-b-2">
        <Header handleLogout={handleLogout} setIsOpen={setIsOpen} isOpen={isOpen} />
      </div>
      <div className="h-full w-full md:flex">
        <Navbar isOpen={isOpen} />
        {children}
      </div>
      <StatusMessageContext.Consumer>
        {({statusMessage}) => 
          <StatusMessage message={statusMessage} />
        }
      </StatusMessageContext.Consumer>
    </>
  )
}