export default function TableHeader({headingNames, children}) {
  return (
    <table className="border-2 divide-y-2 w-full h-fit">
      <thead>
        <tr className="border-2 divide-x-2 w-full">
        {headingNames.map((headName) => 
          <th key={headName} className={`w-1/${headingNames.length} px-2 py-1`}>{headName}</th>
        )}
        </tr>
      </thead>
      <tbody>
        {children}
      </tbody>
    </table>
  )
}