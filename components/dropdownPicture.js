export default function DropdownPicture({isShow, showPopUp, setShowPopUp, handleLogout, userName}) {
  return (
    <>
        <div className={"w-fit flex-col flex flex-nowrap items-center z-10 px-4 pt-4 rounded-md bg-gray-200 divide-y-2 " + (!isShow && "hidden") }>
            <h1 className="w-28 mb-2 text-black-600 font-bold text-center text-sm capitalize">
                { userName }
            </h1>
            <button type="button" className={"py-2 w-full text-center cursor-pointer text-sm font-medium hover:text-red-500 hover:font-bold hover:tracking-wide border-gray-400"} onClick={()=>setShowPopUp(!showPopUp)}>
                Leave Request
            </button>
            <button type="button" className={"py-2 w-full text-center cursor-pointer text-sm font-medium hover:text-red-500 hover:font-bold hover:tracking-wide border-gray-400"} onClick={handleLogout}>
                Sign Out
            </button>
        </div>
    </>
  )
}