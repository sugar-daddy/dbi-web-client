export default function ProduceGoodsForm({produceData, handleChange, handleSubmit}) {

  return (
    <form onSubmit={handleSubmit} className="w-full space-y-4">
      <div className="w-full flex flex-col space-y-2">
        <div className="w-full flex justify-between items-center">
          <label className="w-1/3">ID Material</label>
          <input className="w-2/3 bg-gray-300" type="text" name="materialId" value={produceData.materialId} required readOnly />
        </div>
        <div className="w-full flex justify-between items-center">
          <label className="w-1/3">Name</label>
          <input className="w-2/3 bg-gray-300 capitalize" type="text" name="materialName" value={produceData.materialName} required readOnly />
        </div>
        <div className="w-full flex justify-between items-center">
          <label className="w-1/3">Quantity</label>
          <input className="w-2/3" type="number" name="quantity" value={produceData.quantity} onChange={handleChange} placeholder={`in ${produceData.measurement}`} required />
        </div>
        <div className="w-full flex justify-between items-center">
          <label className="w-1/3">Produce On</label>
          <input className="w-2/3" type="date" name="produceDate" value={produceData.produceDate} onChange={handleChange} required />
        </div>
      </div>
      <button type="submit" className="px-2 border-2 border-black rounded-md hover:bg-gray-300">POST</button>
    </form>
  )
}