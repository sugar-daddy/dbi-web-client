import Image from "next/image";
import { useContext, useEffect, useState } from "react";
import { postBom, putBom } from "../../middleware/bom";
import { getMaterial } from "../../middleware/material";
import { StatusMessageContext } from "../../stores/status-message-context";
import TableHeader from "../tableHeader";
import { checkResponseStatusOK } from "../utilityFunc";

const emptyItem = {
  rawId: '',
  rawName: '',
  rawQuantity: '',
  rawUnit: '',
}
const emptyData = {
  id: 0,
  material_produced: '',
  raws: [
    emptyItem
  ],
}

export default function NewEditBOM({data, isNewEdit, setIsNewEdit, bomData}) {
  const statusMessage = useContext(StatusMessageContext)

  const [materialsList, setMaterialsList] = useState(null)
  const [newData, setNewData] = useState(()=>
    isNewEdit == 'Edit'
    ? {
      id: data.id,
      material_produced: data.material_finished.name,
      raws: data.material_used.map(({id, name, measurement}, idx) => ({
        rawId: id,
        rawName: name,
        rawQuantity: data.quantity[idx],
        rawUnit: measurement,
      })),
    }
    : emptyData
  )
  const handleSubmit = async e => {
    e.preventDefault()
    // post/update data
    console.log(newData)
    if (isNewEdit == 'Edit') {
      const status = await putBom(newData)  // can't update yet, waiting view to finish
      statusMessage.showMessage(status, `Update on BOM of Material ${newData.id}`)
    } else {
      const status = await postBom(newData)
      statusMessage.showMessage(status, `Insert New BOM of Material ${newData.id}`)
    }
    setIsNewEdit(null)
    setNewData(emptyData)
  }
  const handleNameChange = e => {
    const {name, value} = e.target
    const id = materialsList.find(item => item.name==value).id  // error
    setNewData(data => ({
      ...data,
      ['id']: id,
      [name]: value,
    }))
  }
  const handleChangeItems = (e,idx) => {
    const {name, value} = e.target
    const updatedItem = Object.assign({}, newData.raws[idx], {[name]: value})
    if (name === 'rawName')
      updatedItem = Object.assign({}, updatedItem, {
        ['rawId']: materialsList.find(item => item.name==value).id, 
        ['rawQuantity']: 1,
        ['rawUnit']: materialsList.find(item => item.name==value).measurement,
      })
    else if (name === 'rawQuantity' && value < 1)
      updatedItem = Object.assign({}, updatedItem, {['rawQuantity']: 1})
    
    setNewData(data=>({
      ...data,
      raws: [
        ...data.raws.slice(0, idx),
        updatedItem,
        ...data.raws.slice(idx + 1)
      ]
    }))
  }
  const handleAddItems = () => {
    setNewData(data => ({
      ...data,
      raws: [...data.raws, emptyItem]
    }))
  }
  const handleRemoveItems = idx => {
    setNewData(data => ({
      ...data,
      raws: data.raws.filter((_, i) => i != idx)
    }))
  }

  useEffect(async () => {
    const [status, data] = await getMaterial()
    if (checkResponseStatusOK(status)){
      setMaterialsList(data)
    }else
      statusMessage.showMessage(status, 'Fetch Material Data!')
  }, [])
  
  return (
    <form onSubmit={handleSubmit} className="w-full p-5 flex flex-col space-y-10">
      <div className="space-x-2">
        <label>Material Name :</label>
        <select className="capitalize" name="material_produced" value={newData.material_produced} onChange={handleNameChange} required disabled={isNewEdit === 'Edit' ? true : false} >
          <option disabled></option>
          {materialsList && materialsList
            .filter(data => data.type != 'raw' && !bomData.map((bom) => (bom.id)).includes(data.id) || isNewEdit == 'Edit' && data.id === newData.id)
            .map((material, index) => 
              <option key={index} className="capitalize" value={material.name}>{material.name}</option>
          )}
        </select>
      </div>
      <div className="w-full space-y-2">
        <TableHeader headingNames={['ID', 'Name', 'Quantity', 'Unit', 'Action']}>
          {newData && newData.raws.map((data, idx) =>
            <tr key={idx} className="border-b-2 divide-x-2 w-full h-full">
              <td className="px-2 py-1 text-center">
                <input type="text" name="rawId" value={data.rawId} className="w-full text-center text-gray-500 border-0" required readOnly />
              </td>
              <td className="px-2 py-1 text-center">
                <select type="text" name="rawName" value={data.rawName} onChange={e=>handleChangeItems(e,idx)} className="w-full text-center border-0 capitalize" required>
                  <option disabled></option>
                  {materialsList && materialsList
                    .filter(data => data.type != 'finished' && data.id !== newData.id)
                    .map((material, index) =>
                      <option key={index} className="capitalize" value={material.name}>{material.name}</option>
                  )}
                </select>
              </td>
              <td className="px-2 py-1 text-right">
                <input type="number" min={1} name="rawQuantity" value={data.rawQuantity} className="w-full text-center border-0" onChange={e=>handleChangeItems(e,idx)} required />
              </td>
              <td className="px-2 py-1 text-right">
                <input type="text" name="rawUnit" value={data.rawUnit} className="w-full text-center text-gray-500 border-0" required readOnly />
              </td>
              <td className="px-2 py-1 flex justify-center space-x-8 h-full">
                <button type="button" className="rounded-md px-2 hover:bg-gray-200" onClick={()=>handleRemoveItems(idx)}><Image src="/icon-trash.png" alt="" width={20} height={20} /></button>
              </td>
            </tr>
          )}
        </TableHeader>
        <div className="flex flex-row-reverse">
          <button type="button" onClick={handleAddItems}><Image src="/add.svg" alt="" width={20} height={20}/></button>
        </div>
      </div>
      <div className="w-full flex justify-end space-x-2">
        <button type="submit" className="border-2 rounded-md border-black px-1 py-px hover:bg-gray-200">Save</button>
        <button type="button" className="border-2 rounded-md border-black px-1 py-px hover:bg-gray-200" onClick={()=>{setIsNewEdit(null); setNewData(emptyData)}}>Cancel</button>
      </div>
    </form>
  )
}