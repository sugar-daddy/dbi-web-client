import Image from "next/image";
import { useContext, useEffect, useState } from "react";
import SearchInput from "../searchInput";
import TableHeader from "../tableHeader";
import Pagination from "../pagination";
import { deleteBom, getBom } from "../../middleware/bom";
import { StatusMessageContext } from "../../stores/status-message-context";
import { checkResponseStatusOK, sortArrayObjectByProp } from "../utilityFunc";

export default function ViewBOM({setIsNewEdit, setEditData, setBomData}) {
  const statusMessage = useContext(StatusMessageContext)

  const [allBoms, setAllBoms] = useState(null)
  const [pagedData, setPagedData] = useState(null)
  const [filteredData, setFilteredData] = useState(null)  // for search
  
  const handleFetchData = async () => {
    const [status, data] = await getBom()
    if (checkResponseStatusOK(status)) {
      const sorted = sortArrayObjectByProp(data, 'id')
      setAllBoms(sorted)
      setFilteredData(sorted)
      setBomData(sorted)
    } else
      statusMessage.showMessage(status, 'Fetch Data!')
  }
  const handleSearch = input => {
    if (input && +input === +input)
      setFilteredData(allBoms.filter(bom => bom.id == input))
    else if (input && typeof input === 'string')
      setFilteredData(allBoms.filter(bom => bom.material_finished.name.toLowerCase().includes(input)))
    else
      setFilteredData(allBoms)
  }
  const handleGetEdit = bom => {
      setEditData(bom)
      setIsNewEdit('Edit')
  }
  const handleDelete = async id => {
    // delete bom
    const status = await deleteBom(id)
    if (checkResponseStatusOK(status))
      statusMessage.showMessage(status, `Delete BOM with ID ${id}!`)
    else
      statusMessage.showMessage(status, `Delete BOM!`)
    
    // get all bom
    await handleFetchData()
  }

  useEffect(async () => {
    await handleFetchData()
  }, [])

  return (
    <div className="w-full p-5 flex flex-col space-y-10">
      <SearchInput searchFunction={handleSearch} label="BOM" placeholder="Search ID/Name..." />
      <div className="w-full space-y-2">
        <button onClick={()=>setIsNewEdit('New')} className="flex space-x-2 items-center"><Image src="/add.svg" alt="" width={20} height={20} /><span className="border-2 rounded-md border-black px-0.5 hover:bg-gray-200">Add New BOM</span></button>
        { filteredData && 
        <>
          <TableHeader headingNames={['ID', 'Name', 'Action']}>
            {pagedData && pagedData.map(data =>
              <tr key={data.id} className="border-b-2 divide-x-2 w-full h-full">
                <td className="px-2 py-1 text-center">{data.id}</td>
                <td className="px-2 py-1 text-center capitalize">{data.material_finished.name}</td>
                <td className="px-2 py-1 flex justify-center space-x-8 h-full">
                  <button onClick={()=>handleDelete(data.id)}><Image src="/icon-trash.png" alt="" width={20} height={20} /></button>
                  <button onClick={()=>{handleGetEdit(data)}}><Image src="/pencil.svg" alt="" width={20} height={20} /></button>
                </td>
              </tr>
            )}
          </TableHeader>
          <Pagination data={filteredData} setPagedData={setPagedData} itemsPerPage={5} />
        </>
        }
      </div>
    </div>
  )
}