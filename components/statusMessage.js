import { checkResponseStatusOK } from "./utilityFunc"

export default function StatusMessage({ message }) {
  return (
    message && 
    <div className={`${message.text !== '' ? '' : 'hidden'} absolute bottom-0 left-0 w-screen z-30 flex justify-center`}>
      <p className={`w-fit bg-white border-x-2 border-b-0 rounded-lg px-3 py-2 text-center font-semibold capitalize shadow-inner ${checkResponseStatusOK(message.status) ? 'shadow-green-500 text-green-600 border-green-400' : 'shadow-red-500 text-red-600 border-red-400'}`}>
        { (checkResponseStatusOK(message.status) ? 'Successfully ' : 'Failed to ')
          + message.text
        }
      </p>
    </div>
  )
}