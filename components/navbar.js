import React, { useContext, useState } from "react";
import { Transition } from "@headlessui/react";
import DropdownButtonNav from "./dropdownButtonNav";
import { useRouter } from "next/router";
import { UserContext } from "../stores/user-context";

function Navbar({isOpen}) {
    const urlPath = useRouter().pathname.split('/')
    const userContext = useContext(UserContext)
    
    const [moduleShow, setModuleShow] = useState(urlPath[1])
    const modules = [
        {
            name: 'summary',
            moduleParts: [
                {
                    page: 'sales-income',
                    uiName: 'Sales & Income',
                },
                {
                    page: 'expense-outcome',
                    uiName: 'Expense & Outcome',
                },
            ],
            part: ['Admin']
        },
        {
            name: 'finance',
            moduleParts: [
                {
                    page: 'cash-flow',
                    uiName: 'Cash Flow',
                },
                {
                    page: 'invoice',
                    uiName: 'Invoice',
                },
            ],
            part: ['Admin', 'Finance']
        },
        {
            name: 'production',
            moduleParts: [
                {
                    page: 'bom',
                    uiName: 'BOM',
                },
                {
                    page: 'produce-goods',
                    uiName: 'Produce Goods',
                },
            ],
            part: ['Admin', 'Production']
        },
        {
            name: 'personal',
            moduleParts: [
                {
                    page: 'employees',
                    uiName: 'Employees',
                },
                {
                    page: 'working-hours',
                    uiName: 'Working Hours',
                },
                {
                    page: 'leave-requests',
                    uiName: 'Leave Requests',
                },
            ],
            part: ['Admin']
        },
        {
            name: 'inventory',
            moduleParts: [
                {
                    page: 'make-material',
                    uiName: 'Make Material',
                },
                {
                    page: 'manage-materials',
                    uiName: 'Manage Materials',
                },
            ],
            part: ['Admin', 'Inventory']
        },
        {
            name: 'purchasing',
            moduleParts: [
                {
                    page: 'purchase-order',
                    uiName: 'Purchase Order',
                },
                {
                    page: 'add-vendor',
                    uiName: 'Add Vendor',
                },
            ],
            part: ['Admin', 'Purchasing']
        },
        {
            name: 'sales',
            moduleParts: [
                {
                    page: 'sales-order',
                    uiName: 'Sales Order',
                },
                {
                    page: 'add-customer',
                    uiName: 'Add Customer',
                },
            ],
            part: ['Admin', 'Sales']
        },
    ]
    
	return (
        userContext.user &&
        <>
        <div className="bg-red-600 text-white hidden w-48 h-[calc(100vh-5rem)] md:flex md:flex-col ">
            { modules.map(module =>
                <DropdownButtonNav
                    moduleName={module.name}
                    moduleParts={module.moduleParts}
                    moduleShow={moduleShow}
                    setModuleShow={setModuleShow}
                    urlPath={urlPath}
                    key={module.name}
                    disabled={!module.part.includes(userContext.user.userType)}
                />
            )}
        </div>
        <Transition
            show={isOpen}
            enter="transition ease-out duration-100 transform"
            enterFrom="opacity-0 -translate-y-10 scale-95"
            enterTo="opacity-100 -translate-y-0 scale-100"
            leave="transition ease-in duration-75 transform"
            leaveFrom="opacity-100 -translate-y-0 scale-100"
            leaveTo="opacity-0 -translate-y-10 scale-95"
            className="bg-red-600 text-white flex flex-col w-full absolute z-20 md:hidden"
        >
            { modules.map(module =>
                <DropdownButtonNav
                    moduleName={module.name}
                    moduleParts={module.moduleParts}
                    moduleShow={moduleShow}
                    setModuleShow={setModuleShow}
                    urlPath={urlPath}
                    key={module.name}
                    disabled={!module.part.includes(userContext.user.userType)}
                />
            )}
        </Transition>
        </>
	);
}

export default Navbar;