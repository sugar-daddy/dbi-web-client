import Image from "next/image";
import { useContext, useEffect, useState } from "react";
import TableHeader from "../tableHeader"
import Pagination from "../pagination";
import { getEmployee } from "../../middleware/employee";
import { StatusMessageContext } from "../../stores/status-message-context";
import { checkResponseStatusOK } from "../utilityFunc";

export default function ViewEmployees({setAddNewEmployees, setEditData}) {
  const [pagedData, setPagedData] = useState(null)
  const [employeesData, setEmployeesData] = useState(null)
  const statusMessage = useContext(StatusMessageContext)

  const handleGetData = async() => {
    const [status,dataMaterialRequest] = await getEmployee()
    if (checkResponseStatusOK(status))
      setEmployeesData(dataMaterialRequest)
    else
      statusMessage.showMessage(status, 'Fetch Data!')
  }

  useEffect(async() =>{
    await handleGetData()
  },[])  

  return (
      <div className="p-5 space-y-2 w-full text-lg">
        <button className="flex items-center" onClick={()=>{setAddNewEmployees(true)}} type="button">
          <div className="border-black border-2 ml-2 rounded h-[26px]">
            <Image src="/add.svg" alt="" width={20} height={20}/>
          </div>
          <span className="rounded px-1">Add New Employee</span>
        </button> 
        { employeesData &&
          <>
            <TableHeader headingNames={['Name', 'Part', 'Salary', 'E-mail']}>
              {pagedData && pagedData
                .map((data) => 
                <tr key={data.id} className="border-b-2 divide-x-2 w-full h-full">
                  <td className="px-2 py-1 text-center capitalize">{data.name}</td>
                  <td className="px-2 py-1 text-center capitalize">{data.part}</td>
                  <td className="px-2 py-1 text-center">{data.salary}</td>
                  <td className="px-2 py-1 text-center">{data.email}</td>
                </tr>
              )}
            </TableHeader>
            <Pagination data={employeesData} setPagedData={setPagedData} itemsPerPage={5}/>
          </>
        }
      </div>
  )
}