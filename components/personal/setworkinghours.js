import TableHeader from "/components/tableHeader"

export default function SetWorkingHours({handlerchange, monday_in, tuesday_in, wednesday_in, thursday_in, friday_in, monday_out, tuesday_out, wednesday_out, thursday_out, friday_out, handlerSubmit}) {

  return (
    <div className="w-full flex justify-center items-center">
      <TableHeader headingNames={['Mo', 'Tu', 'We', 'Th','Fr']}>
        <tr className="border-b-2 divide-x-2 w-fit h-full bg-[#EDCDBB]">
            <td className="px-2 py-1 text-center w-10"><input required type="text" onChange={handlerchange} value={monday_in} name="monday_in" placeholder="Start Time" ></input></td>
            <td className="px-2 py-1 text-center w-10"><input required type="text" onChange={handlerchange} value={tuesday_in} name="tuesday_in" placeholder="Start Time"></input></td>
            <td className="px-2 py-1 text-center w-10"><input required type="text" onChange={handlerchange} value={wednesday_in} name="wednesday_in" placeholder="Start Time"></input></td>
            <td className="px-2 py-1 text-center w-10"><input required type="text" onChange={handlerchange} value={thursday_in} name="thursday_in" placeholder="Start Time"></input></td>
            <td className="px-2 py-1 text-center w-10"><input required type="text" onChange={handlerchange} value={friday_in} name="friday_in" placeholder="Start Time"></input></td>
        </tr>
        <tr className="border-b-2 divide-x-2 w-fit h-full bg-[#153B44]">
            <td className="px-2 py-1 text-center w-10"><input required type="text" onChange={handlerchange} value={monday_out} name="monday_out" placeholder="End Time"></input></td>
            <td className="px-2 py-1 text-center w-10"><input required type="text" onChange={handlerchange} value={tuesday_out} name="tuesday_out" placeholder="End Time"></input></td>
            <td className="px-2 py-1 text-center w-10"><input required type="text" onChange={handlerchange} value={wednesday_out} name="wednesday_out" placeholder="End Time"></input></td>
            <td className="px-2 py-1 text-center w-10"><input required type="text" onChange={handlerchange} value={thursday_out} name="thursday_out" placeholder="End Time"></input></td>
            <td className="px-2 py-1 text-center w-10"><input required type="text" onChange={handlerchange} value={friday_out} name="friday_out" placeholder="End Time"></input></td>
        </tr>
      </TableHeader>
      <div className="w-full items-center">
        <button onClick={handlerSubmit} className="px-2 py-1 border-2 border-black rounded-md hover:bg-gray-200" type="button">OK</button>
      </div>
    </div>
  )
}