import React, { useContext, useState } from 'react';
import { postRequestLeave } from '../../middleware/request-leave';
import { StatusMessageContext } from '../../stores/status-message-context';
import { UserContext } from '../../stores/user-context';
import { checkResponseStatusOK } from '../utilityFunc';

export default function LeaveRequest({setShowPopUp}) {
    const statusMessage = useContext(StatusMessageContext)
    const user = useContext(UserContext).user
    const leaveType = ['Sick Leave', 'Paid Leave']
    const [request, setRequest] = useState({
        type: '',
        start_date: '',
        end_date: '',
        employee_id: user.id,
    })
    
    const handleSubmit = async (e) => {
        e.preventDefault()
        // post request
        const status = await postRequestLeave(request)
        if (checkResponseStatusOK(status)) {
            setShowPopUp(false)
        }
        statusMessage.showMessage(status, 'Request Leave!')
    }

    const handleChange = (e) => {
        const {name, value} = e.target
        setRequest(old => ({
            ...old,
            [name]: value,
        }))
    }

    return (
        <div className="w-full flex justify-center items-center">
            <form onSubmit={handleSubmit} className="w-full grid grid-cols-1 grid-rows-4 space-y-3">
                <div className="w-full flex-end flex items-center space-x-14">
                    <label htmlFor="type" className="block text-sm font-medium">Type</label>
                    <select name="type" onChange={handleChange} value={request.type} className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full">
                        <option disabled/>
                        {leaveType.map(type => 
                            <option key={type} value={type}>{type}</option>
                        )}
                    </select>
                </div>
                <div className="w-fit flex-end flex items-center space-x-6">
                    <label htmlFor="start_date" className="block text-sm font-medium">Start Date</label>
                    <input type="date" name="start_date" onChange={handleChange} value={request.start_date} className="" />
                </div>
                <div className="w-fit flex-end flex items-center space-x-8">
                    <label htmlFor="end_date" className="block text-sm font-medium">End Date</label>
                    <input type="date" name="end_date" onChange={handleChange} value={request.end_date} className="" />
                </div>
                <div className="w-full flex justify-end space-x-5">
                    <button type="submit" className="text-black bg-gray-100 hover:bg-gray-400 rounded-lg px-4">Request</button>
                    <button type="button" onClick={()=>setShowPopUp(false)} className="text-black bg-gray-100 hover:bg-gray-400 rounded-lg px-4">Cancel</button>
                </div>
            </form>
        </div>
    )
}