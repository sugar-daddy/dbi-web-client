export default function SetAccount({ handleChange, username, password, handlerSubmit}) {

  return (
    <div className="w-full flex justify-center items-center">
      <form className="w-full grid grid-cols-1 grid-rows-4 space-y-3">
        <div className="w-full justify-between flex items-center">
          <label className="w-1/3">Username</label>
          <input required className="w-2/3" type="text" name="username" value={username} onChange={handleChange} />
        </div>
        <div className="w-full justify-between flex items-center">
          <label className="w-1/3">Password</label>
          <input required className="w-2/3" type="password" name="password" value={password} onChange={handleChange} />
        </div>
        <div className="w-full items-center">
          <button onClick={handlerSubmit} className="px-2 py-1 border-2 border-black rounded-md hover:bg-gray-200" type="button">OK</button>
        </div>
      </form>
    </div>
  )
}