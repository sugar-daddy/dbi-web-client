import {useContext, useState} from "react";
import { postEmployee } from "../../middleware/employee";
import { StatusMessageContext } from "../../stores/status-message-context";
import PopUp from "../popUp";
import SetAccount from "./setaccount";
import SetWorkingHours from "./setworkinghours";

export default function AddNewEmployees({editData, setAddNewEmployees}) {
    const refreshData = {
        username: '',
        password: '',
        name: '',
        email: '',
        birthday: '',
        education: '',
        address: '',
        country: '',
        region: '',
        postalCode: '',
        city: '',
        part: '',
        salary: 0,
        monday_in: '',
        tuesday_in: '',
        wednesday_in: '',
        thuesday_in: '',
        friday_in: '',
        monday_out: '',
        tuesday_out: '',
        wednesday_out: '',
        thuesday_out: '',
        friday_out: '',
    }
    const [newData, setNewData] = useState(() => editData ? editData : refreshData)
    const statusMessage = useContext(StatusMessageContext)

    
    const handleSubmit= async e => {
        e.preventDefault()
        const status =  await postEmployee(newData)
        const text = `Insert new Employee !`
        statusMessage.showMessage(status, text)
        setNewData(refreshData)
    }

    const handleChange = e => {
        const {name, value} = e.target
        setNewData(data => ({
          ...data,
          [name]: value,
        }))
    }

    const [showSetAccount, setShowSetAccount] = useState(false)
    const [showSetWorkingHour, setShowSetWorkingHour] = useState(false)

    return(
        <>
        <div className="px-5 space-y-2 w-full">
            <form className="py-2 w-full" onSubmit={handleSubmit}>
                <div className="py-2 w-full flex space-x-2">
                    <label className=" w-1/12">Name</label >
                    <input required onChange={handleChange} className=" w-10/12 bg-gray-50 focus:bg-slate-200" type="text" name="name" value={newData.name}></input>
                </div>
                <div className="py-2 w-full flex space-x-2">
                    <label className=" w-1/12" >E-mail</label >
                    <input required onChange={handleChange} className=" w-10/12 bg-gray-50 focus:bg-slate-200" type="text" name="email" value={newData.email}></input>
                </div>
                <div className="py-2 w-full flex space-x-2">
                    <label className=" w-1/12">Birth Day</label >
                    <input required onChange={handleChange} type="date" name="birthday" className=" w-4/12 bg-gray-50 focus:bg-slate-200" value={newData.birthday}></input>
                    <label  className=" w-1/12">Education</label >
                    <input required onChange={handleChange} className=" w-4/12 bg-gray-50 focus:bg-slate-200" type="text" name="education" value={newData.education}></input>
                </div>
                <div className="py-2 w-full flex space-x-2">
                    <label className="w-1/12">Street/House Number</label >
                    <input required onChange={handleChange} className=" w-10/12 bg-gray-50 focus:bg-slate-200" type="text" name="address" value={newData.address}></input>
                </div>
                <div className="py-2 w-full flex space-x-2">
                    <label className=" w-1/12">Country</label >
                    <input required onChange={handleChange} className=" w-4/12 bg-gray-50 focus:bg-slate-200" type="text" name="country" value={newData.country}></input>
                    <label  className=" w-1/12">Region</label >
                    <input required onChange={handleChange} className=" w-4/12  bg-gray-50 focus:bg-slate-200" type="text" name="region" value={newData.region}></input>
                </div>
                <div className="py-2 w-full flex space-x-2">
                    <label className="w-1/12">Postal Code</label >
                    <input required onChange={handleChange} className=" w-4/12 bg-gray-50 focus:bg-slate-200" type="text" name="postalCode" placeholder="Postal Code"value={newData.postalCode}></input>
                    <label className=" w-1/12">City</label >
                    <input required onChange={handleChange} className=" w-4/12 bg-gray-50 focus:bg-slate-200" type="text" name="city" placeholder="City"value={newData.city}></input>
                </div>
                <div className="py-2 w-full flex space-x-2">
                    <label className=" w-1/12">Part</label >
                    <select required onChange={handleChange} className=" w-10/12 bg-gray-50 focus:bg-slate-200" type="text" name="part" value={newData.part}>
                        <option disabled></option>
                        <option className="" value="sales">Sales</option>
                        <option className="" value="purchasing">Purchasing</option>
                        <option className="" value="inventory">Inventory</option>
                        <option className="" value="finance">Finance</option>
                        <option className="" value="production">Production</option>
                    </select> 
                </div>
                <div className="py-2 w-full flex space-x-2">
                    <label className=" w-1/12" >Salary</label >
                    <input required onChange={handleChange} className=" w-10/12 bg-gray-50 focus:bg-slate-200" type="number" name="salary" value={newData.salary}></input>
                </div>
                <button type="button" onClick={()=>{setShowSetAccount(true)}} className="mt-5 mr-4 bg-white hover:bg-red-700 border-black hover:border-red-700 text-xl border-2 text-black py-1 px-7 rounded">Set Account</button>
                <button type="button" onClick={()=>{setShowSetWorkingHour(true)}} className="mt-5 mr-10 bg-white hover:bg-red-700 border-black hover:border-red-700 text-xl border-2 text-black py-1 px-7 rounded">Set Working Hours</button>
                <button type="submit" className="mt-5 mr-4 bg-white hover:bg-red-700 border-black hover:border-red-700 text-xl border-2 text-black py-1 px-7 rounded">SAVE</button>
                <button onClick={()=>setAddNewEmployees(false)} className="mt-2 bg-white hover:bg-red-700 border-black hover:border-red-700 text-xl border-2 text-black py-1 px-7 rounded" type="button">BACK</button>
            </form>
        </div>
        <PopUp showPopUp={showSetAccount} setShowPopUp={setShowSetAccount} title={`Add Employees`}>
            <SetAccount handleChange={handleChange} username={ newData.username } password={newData.password} handlerSubmit={()=> setShowSetAccount(false)}/>
        </PopUp>
        <PopUp showPopUp={showSetWorkingHour} setShowPopUp={setShowSetWorkingHour} title={`Set Hours`}>
            <SetWorkingHours handlerchange={handleChange} monday_in={newData.monday_in} tuesday_in={newData.tuesday_in}  wednesday_in={newData.wednesday_in}  thuesday_in={newData.thuesday_in} friday_in={newData.friday_in} 
            monday_out={newData.monday_out} tuesday_out={newData.tuesday_out}  wednesday_out={newData.wednesday_out}  thuesday_out={newData.thuesday_out} friday_out={newData.friday_out}
            handlerSubmit={()=> setShowSetWorkingHour(false)} 
            />
        </PopUp>
    </>
    )
}