export default function PopUp({showPopUp, setShowPopUp, title, children}) {
    return(
        showPopUp && 
        <div className="absolute top-0 left-0 flex items-center justify-center w-screen h-screen overflow-x-hidden overflow-y-hidden z-50 " id="popup-modal">
            <div onClick={() => setShowPopUp(false)} className="absolute top-0 left-0 w-full h-full bg-slate-300 opacity-20" />
            <div className="relative border-black border-2 rounded-lg p-1 w-1/3 min-w-fit max-w-md h-fit bg-gray-100 z-[51]">
                <div className="relative bg-white rounded-lg px-4 py-2 shadow dark:bg-gray-700">
                    <div className="flex justify-center px-2 pb-2">
                        {title ? <h1>{title}</h1> : ''}
                        <button type="button" onClick={()=>setShowPopUp(!showPopUp)} className="text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 ml-auto inline-flex items-center dark:hover:bg-gray-800 dark:hover:text-white" data-modal-toggle="popup-modal">
                            <svg className="w-5 h-5" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fillRule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clipRule="evenodd"></path></svg>  
                        </button>
                    </div>
                    {children}
                </div>
            </div>
        </div> 
    )   
}