import Image from "next/image";
import { useState } from "react";

export default function SearchInput({searchFunction, label, placeholder}) {
  const [input, setInput] = useState('')
  const handleChange = e => {
    setInput(e.target.value)
  }
  
  return (
    <div className="w-full space-x-2 flex justify-center items-center">
      <label>{label}</label>
      <div className="w-1/2 h-fit ring-1 ring-gray-500 focus:ring-gray-800 flex justify-between items-center divide-x-2">
        <input className="px-2 py-1 w-[calc(100%-1.25rem)] border-0 focus:ring-0" type="text" value={input} name="searchInput" onChange={handleChange} placeholder={placeholder}/>
        {input &&
          <button className="px-1.5 py-1 border-gray-800 hover:bg-gray-200" onClick={()=>{setInput(''); searchFunction('')}}><svg className="w-5 h-5" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fillRule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clipRule="evenodd"></path></svg></button>
        }
      </div>
      <button className="px-1.5 pt-1 pb-px border-2 rounded-md border-black hover:bg-gray-200" onClick={()=>searchFunction(input)}><Image src="/search icon.png" alt="" width={20} height={20} /></button>
    </div>
  )
}