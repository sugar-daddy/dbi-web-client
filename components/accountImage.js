import Image from "next/image"

function ImageAccount(){
    return(
        <div style={{borderRadius: '5px', overflow: 'hidden', cursor : "pointer"}}>
            <Image src="/settings.svg" width="30" height="30" objectFit="cover" />
        </div>
    ) 
}

export default ImageAccount
