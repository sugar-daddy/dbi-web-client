import React, { useContext, useState } from "react";
import ImageAccount from "./accountImage";
import DropdownPicture from "./dropdownPicture";
import LeaveRequest from "./personal/leaveRequest";
import PopUp from "./popUp";
import { UserContext } from "../stores/user-context";
import Hamburger from "./hamburger";

function Header({handleLogout, setIsOpen, isOpen}) {

	const userContext = useContext(UserContext)
	const [isShow, setIsShow] = useState(false)
	const [showPopUp, setShowPopUp ] = useState(false)

	return (
		<>
			<div className="justify-between flex items-center flex-shrink-0 w-full flex-row relative">
				<div className="w-3/4 flex flex-row items-center sm:w-fit">
					<button
						onClick={() => setIsOpen(!isOpen)}
						type="button"
						className="bg-red-600 md:hidden inline-flex items-center mx-5 justify-center p-2 rounded-md text-white  hover:bg-blue-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-blue-800 focus:ring-white"
						aria-controls="mobile-menu"
						aria-expanded="false"        
					>
						<Hamburger isOpen={isOpen}/>
					</button>
					<h1 className=" text-red-600 font-bold text-xl">
						Donut Business International
					</h1>
				</div>
				{	userContext.user &&
					<div className="flex flex-col absolute top-0 right-0 space-y-4 w-fit ">
						<div className="w-full flex justify-end items-center space-x-3" onClick={()=>setIsShow(!isShow)}>
							<h1 className="text-black-600 font-bold text-sm capitalize hidden md:inline">
								Hi, { userContext.user.name }
							</h1>
							<ImageAccount/>
						</div>
						<DropdownPicture isShow={isShow} showPopUp={showPopUp} setShowPopUp={setShowPopUp} handleLogout={handleLogout} userName={userContext.user.name} />
					</div>
				}
			</div>
			<PopUp showPopUp={showPopUp} setShowPopUp={setShowPopUp} title={"Leave Request"}>
				<LeaveRequest setShowPopUp={setShowPopUp} />
			</PopUp>
		</>
	);
}

export default Header;