export function formatNumberCurrency(val) {
  return (val).toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ",") + '.00'
}

export function formatDatetimeToDate(datetime) {
  return datetime.split('T')[0]
}

export function formatDatetimeToTime(datetime){
  return datetime.split('T')[1].slice(0,-1)
}

export function checkResponseStatusOK(status) {
  return status >= 200 && status < 300
}

export function sortArrayObjectByProp(arr, prop, order=-1) { // order=1 -> ASC, order = -1 -> DESC
  return arr.sort((a,b) => (a[prop] > b[prop]) ? order : ((b.date > a.date) ? -order : 0))
}

export function sumPropValueOfArrayObject(arr, prop) {
  return arr.map(obj => (obj[prop])).reduce((prev, next) => (prev + next))
}