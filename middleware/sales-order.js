import { API } from './baseAPI'
const querystring = require('querystring')

const endpoint = '/sales_order'

const convertSalesOrder = (SalesOrder) => {
  const customer_id = SalesOrder.customer_id
  const employee_id = SalesOrder.employee_id
  let materialid = ''
  let quantityid = ''
  let priceid = ''

  SalesOrder.raws.map(({material_id, quantity, price}) => {
    materialid += material_id + ','
    quantityid += quantity + ','
    priceid += price + ','

  })
  materialid = materialid.slice(0,-1)
  quantityid = quantityid.slice(0,-1)
  priceid = priceid.slice(0,-1)
  
  return {customer_id, material_id: materialid, quantity: quantityid, price: priceid, employee_id}
}

export async function getSalesOrder(filter = {}) {
  let status = 0
  let data = {}

  await API.get(endpoint, {
    params: filter,
  })
  .then(res => {
    // console.log(res)
    status = res.status
    data = res.data.data
  })
  .catch(err => {
    // console.log(err)
    status = err.response.status
  })

  return [status, data]
}

export async function postSalesOrder(SalesOrder) {
  let status = 0
  const convert = convertSalesOrder(SalesOrder)
  const data = querystring.stringify(convert)
  
  await API.post(endpoint, data)
  .then(res => {
    // console.log(res)
    status = res.status
  })
  .catch(err => {
    // console.log(err)
    status = err.response.status
  })

  return status
}