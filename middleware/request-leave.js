import { API } from './baseAPI'
const querystring = require('querystring')

const endpoint = '/request_leave'

export async function postRequestLeave(requestLeave) {
  let status = 0
  const data = querystring.stringify(requestLeave)

  await API.post(endpoint, data)
  .then(res => {
    // console.log(res)
    status = res.status
  })
  .catch(err => {
    // console.log(err)
    status = err.response.status
  })

  return status
}

export async function getRequestLeave(filter = {}) {
  let data = {}
  let status = 0

  await API.get(endpoint, {
    params: filter,
  })
  .then(res => {
    // console.log(res)
    data = res.data.data
    status = res.status
  })
  .catch(err => {
    // console.log(err)
    status = err.response.status
  })

  return [status, data]
}


export async function putRequestLeave(updateRequestLeave){
  let status = 0
  const data = querystring.stringify(updateRequestLeave)

  await API.put(endpoint, data)
  .then(res => {
    // console.log(res)
    status = res.status
  })
  .catch(err => {
    // console.log(err)
    status = err.response.status
  })

  return status
}