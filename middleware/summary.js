import { API } from './baseAPI'

export async function getSalesIncome() {
  let status = 0
  let data = {}

  await API.get('/summary/sales-invoice-summary')
  .then(res => {
    // console.log(res)
    status = res.status
    data = res.data.data
  })
  .catch(err => {
    // console.log(err)
    status = err.response.status
  })

  return [status, data]
}

export async function getExpenseOutcome() {
  let status = 0
  let data = {}

  await API.get('/summary/expense-summary')
  .then(res => {
    // console.log(res)
    status = res.status
    data = res.data.data
  })
  .catch(err => {
    // console.log(err)
    status = err.response.status
  })

  return [status, data]
}