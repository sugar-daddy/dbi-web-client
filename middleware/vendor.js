import { API } from './baseAPI'
const querystring = require('querystring')

const endpoint = '/procurement'
const endpointget = '/vendor'

export async function getVendor(filter = {}) {
  let data = {}
  let status = 0

  await API.get(endpointget, {
    params: filter,
  })
  .then(res => {
    // console.log(res)
    data = res.data.data
    status = res.status
  })
  .catch(err => {
    // console.log(err)
    status = err.response.status
  })

  return [status, data]
}

const convertAddVendor = (addVendor) => {
  const name = addVendor.name
  const address = addVendor.address
  const country = addVendor.country
  const region = addVendor.region
  const postalcode = addVendor.postalcode
  const city = addVendor.city
  
  return {name, address, country, region, 'postal-code': postalcode, city}
}

export async function postAddVendor(addVendor) {
  let status = 0
  const convert = convertAddVendor(addVendor)
  const data = querystring.stringify(convert)

  await API.post(endpoint+'/vendor', data)
  .then(res => {
    // console.log(res)
    status = res.status
  })
  .catch(err => {
    // console.log(err)
    status = err.response.status
  })

  return status
}