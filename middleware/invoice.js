import { API } from './baseAPI'
const querystring = require('querystring')

export async function postInvoice(invoice, ref) {
  let status = 0
  const data = querystring.stringify(invoice)
  
  let endpoint = '/sales_invoice'
  if (ref == 'PO')
    endpoint = '/purchase_invoice'
  
  await API.post(endpoint, data)
  .then(res => {
    // console.log(res)
    status = res.status
  })
  .catch(err => {
    // console.log(err)
    status = err.response.status
  })

  return status
}
