import { API } from "./baseAPI"
const querystring = require("querystring")

const extractAuthUser = (data) => {
  const part = data.part.charAt(0).toUpperCase() + data.part.slice(1)
  return {id: data.id, name: data.name, userType: part}
}

export async function fetchLogin(username, password) {
  let status = 0
  let user = {}
  const data = querystring.stringify({
    'username': username,
    'password': password,
  })
  
  await API.post('/login', data)
  .then(res => {
    // console.log(res)
    status = res.status
    user = extractAuthUser(res.data.data)
  })
  .catch(err => {
    // console.log(err)
    status = err.response.status
  })

  return [status, user]
}

export async function fetchCookie() {
  let status = 0
  let data = {}

  await API.get('/verify')
  .then(res => {
    // console.log(res)
    status = res.status
    data = extractAuthUser(res.data.data)
  })
  .catch(err => {
    // console.log(err)
    status = err.response.status  
  })

  return [status, data]
}

export async function fetchLogout() {
  let status = 0

  await API.post('/logout')
  .then(res => 
    status = res.status
  )
  .catch(err => 
    status = err.response.status
  )

  return status
}