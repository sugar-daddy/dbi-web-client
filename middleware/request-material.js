import { API } from './baseAPI'
const querystring = require('querystring')

const endpoint = '/reqmaterial'

export async function getRequestMaterial(filter = {}) {
  let data = {}
  let status = 0

  await API.get(endpoint, {
    params: filter,
  })
  .then(res => {
    // console.log(res)
    data = res.data.data
    status = res.status
  })
  .catch(err => {
    // console.log(err)
    status = err.response.status
  })

  return [status, data]
}

export async function putRequestMaterial(updateRequestMaterial){
    let status = 0
    const data = querystring.stringify(updateRequestMaterial)
  
    await API.put(endpoint+ "/"+ updateRequestMaterial.id, data)
    .then(res => {
      // console.log(res)
      status = res.status
    })
    .catch(err => {
      // console.log(err)
      status = err.response.status
    })
  
    return status
  }