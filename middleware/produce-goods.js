import { API } from './baseAPI'
const querystring = require('querystring')

const endpoint = '/production/request'

export async function postProduceGoods(goods) {
  let status = 0
  const payload = {
    id: goods.materialId,
    quantity: goods.quantity,
    produce_date: goods.produceDate,
    employee_id: goods.employee_id,
  }
  const data = querystring.stringify(payload)

  await API.post(endpoint, data)
  .then(res => {
    // console.log(res)
    status = res.status
  })
  .catch(err => {
    // console.log(err)
    status = err.response.status
  })

  return status
}
