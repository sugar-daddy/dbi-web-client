import { API } from './baseAPI'
const querystring = require('querystring')

const endpoint = '/cash_report'

export async function getCashFlow(params = {}) {
  let status = 0
  let data = {}

  await API.get(endpoint, {
    params: params,
  })
  .then(res => {
    // console.log(res)
    data = res.data.data
    status = res.status
  })
  .catch(err => {
    // console.log(err)
    status = err.response.status
  })

  return [status, data]
}

export async function postCashFlow(cashflow) {
  let status = 0
  const data = querystring.stringify(cashflow)

  await API.post(endpoint, data)
  .then(res => {
    // console.log(res)
    status = res.status
  })
  .catch(err => {
    // console.log(err)
    status = err.response.status
  })

  return status
}

export async function putCashFlow(cashflow) {
  let status = 0
  const data = querystring.stringify(cashflow)

  await API.put(endpoint, data)
  .then(res => {
    // console.log(res)
    status = res.status
  })
  .catch(err => {
    // console.log(err)
    status = err.response.status
  })

  return status
}