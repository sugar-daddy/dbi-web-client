import { fetchCookie } from './authentication'
import { API, APIBACKUP } from './baseAPI'
const querystring = require('querystring')

const endpoint = '/procurement'

    export async function getPurchaseOrder(filter = {}) {
    let status = 0
    let data = {}
  
    await API.get(endpoint, {
      params: filter,
    })
    .then(res => {
      // console.log(res)
      status = res.status
      data = res.data.data
    })
    .catch(err => {
      // console.log(err)
      status = err.response.status
    })
  
    return [status, data]
  }


  const convertProcurement = (itemPO) => {
    const vendor_id = itemPO.vendor_id
    const employee_id = itemPO.employee_id
    let materialid = ''
    let quantityid = ''
    let priceid = ''

    itemPO.raws.map(({material_id, quantity, price}) => {
      materialid += material_id + ','
      quantityid += quantity + ','
      priceid += price + ','

    })
    materialid = materialid.slice(0,-1)
    quantityid = quantityid.slice(0,-1)
    priceid = priceid.slice(0,-1)
    
    return {vendor_id:vendor_id, material_id: materialid, quantity: quantityid, price: priceid, employee_id}
  }

  export async function postProcurement(itemPO) {
    let status = 0
    const convert = convertProcurement(itemPO)
    const data = querystring.stringify(convert)
    
    await fetchCookie()

    await API.post(endpoint, data)
    .then(res => {
      // console.log(res)
      status = res.status
    })
    .catch(err => {
      // console.log(err)
      status = err.response.status
    })
  
    return status
  }
