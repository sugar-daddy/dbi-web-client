import { API } from './baseAPI'
const querystring = require('querystring')

const endpoint = '/production'

const convertBOMPayload = (bom) => {
  const id = bom.id
  let using = ''
  let quantity = ''

  bom.raws.map(({rawId, rawQuantity}) => {
    using += rawId + ','
    quantity += rawQuantity + ','
  })
  using = using.slice(0,-1)
  quantity = quantity.slice(0,-1)
  
  return {id, using, quantity}
}

export async function getBom(id = null) {
  let status = 0
  let data = {}

  await API.get(endpoint, {
    params: { id: id }
  })
  .then(res => {
    // console.log(res)
    data =  res.data.data
    status = res.status
  })
  .catch(err => {
    // console.log(err)
    status = err.response.status
  })

  return [status, data]
}

export async function postBom(bom) {
  let status = 0
  const convert = convertBOMPayload(bom)
  const data = querystring.stringify(convert)
  
  await API.post(endpoint, data)
  .then(res => {
    // console.log(res)
    status = res.status
  })
  .catch(err => {
    // console.log(err)
    status = err.response.status
  })

  return status
}

export async function putBom(bom) {
  let status = 0
  const convert = convertBOMPayload(bom)
  const data = querystring.stringify(convert)

  // console.log(bom)
  await API.put(endpoint, data)
  .then(res => {
    // console.log(res)
    status = res.status
  })
  .catch(err => {
    // console.log(err)
    status = err.response.status
  })

  return status
}

export async function deleteBom(id) {
  let status = 0
  
  await API.delete(endpoint, {
    params: { id }
  })
  .then(res => {
    // console.log(res)
    status = res.status
  })
  .catch(err => {
    // console.log(err)
    status = err.response.status
  })

  return status
}
