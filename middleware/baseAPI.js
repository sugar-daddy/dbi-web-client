const axios = require('axios')

export const API = axios.create({
  withCredentials: true,
  credentials: 'include',
  baseURL: 'http://127.0.0.1:8000',
})