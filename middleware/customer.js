import { API } from './baseAPI'
const querystring = require('querystring')

const endpoint = '/customer'

export async function postMakeCustomer(makeCustomer) {
  let status = 0
  const data = querystring.stringify(makeCustomer)

  await API.post(endpoint, data)
  .then(res => {
    // console.log(res)
    status = res.status
  })
  .catch(err => {
    // console.log(err)
    status = err.response.status
  })

  return status
}

export async function getCustomer(filter = {}) {
  let data = {}
  let status = 0

  await API.get(endpoint, {
    params: filter,
  })
  .then(res => {
    // console.log(res)
    data = res.data.data
    status = res.status
  })
  .catch(err => {
    // console.log(err)
    status = err.response.status
  })

  return [status, data]
}