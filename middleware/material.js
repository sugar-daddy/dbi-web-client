import { API } from './baseAPI'
const querystring = require('querystring')

const endpoint = '/material'

export async function getMaterial(filter = {}) {
  let data = {}
  let status = 0

  await API.get(endpoint, {
    params: filter,
  })
  .then(res => {
    // console.log(res)
    data = res.data.data
    status = res.status
  })
  .catch(err => {
    // console.log(err)
    status = err.response.status
  })

  return [status, data]
}

export async function postMakeMaterial(makeMaterial) {
  let status = 0
  const data = querystring.stringify(makeMaterial)

  await API.post(endpoint, data)
  .then(res => {
    // console.log(res)
    status = res.status
  })
  .catch(err => {
    // console.log(err)
    status = err.response.status
  })

  return status
}

export async function putMaterial(material) {
  let status = 0
  const data = querystring.stringify(material)

  await API.put(endpoint, data)
  .then(res => {
    // console.log(res)
    status = res.status
  })
  .catch(err => {
    // console.log(err)
    status = err.response.status
  })

  return status
}