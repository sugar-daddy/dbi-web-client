import React from "react"

export const StatusMessageContext = React.createContext({
  statusMessage: {status: 0, text: ''},
  setStatusMessage: () => {},
  showMessage: () => {}
})

