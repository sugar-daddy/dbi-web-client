import { useState } from "react";
import MainLayout from "/components/mainLayout";
import CheckMaterials from "../../components/inventory/check-materials";
import RequestMaterials from "../../components/inventory/request-materials";

export default function ManageMaterials() {
  const [isOpen, setIsOpen] = useState(false)
  const [isRequestMaterials, setRequestMaterials] = useState(false)
  const [editData, setEditData] = useState(null)

  return (
    <MainLayout isOpen={isOpen} setIsOpen={setIsOpen}>
      {isRequestMaterials
      ? <RequestMaterials data={editData} setRequestMaterials={setRequestMaterials}/>
      : <CheckMaterials setRequestMaterials={setRequestMaterials} setEditData={setEditData} />
      }
    </MainLayout>
  )
}