import { data } from "autoprefixer";
import {useContext, useState} from "react";
import { postMakeMaterial } from "../../middleware/material";
import { StatusMessageContext } from "../../stores/status-message-context";
import MainLayout from "/components/mainLayout";

export default function MakeMaterial() {
    const [isOpen, setIsOpen] = useState(false)
    const statusMessage = useContext(StatusMessageContext)
    const refreshData = {
        measurement: "",
        name: "",
        price: 0,
        quantity: 0,
        type: "",
    }

    const handleSubmit= async e => {
        e.preventDefault()
        const status =  await postMakeMaterial(newMakeMaterials)
        const text = `Insert new Material !`
        statusMessage.showMessage(status, text)
        setNewMakeMaterials(refreshData)
    }

    const [newMakeMaterials, setNewMakeMaterials] = useState(refreshData)

    const handleChange = e => {
        const {name, value} = e.target
        setNewMakeMaterials(data => ({
          ...data,
          [name]: value
        }))
    }

    return(
        <MainLayout isOpen={isOpen} setIsOpen={setIsOpen}>
            <div className="p-5 space-y-2 w-full">
                <form onSubmit={handleSubmit} className="py-2 w-full text-lg">
                    <div className="py-2 w-full flex space-x-2">
                        <label className="w-[13%]">Material Name</label >
                        <input required onChange={handleChange} className=" w-9/12 bg-gray-50 focus:bg-slate-200" type="text" name="name" value={newMakeMaterials.name}></input>
                    </div>
                    <div className="py-2 w-full flex space-x-2">
                        <label className="w-[13%]">Type</label >
                        <select required onChange={handleChange} className=" w-9/12 bg-gray-50 focus:bg-slate-200" type="text" name="type" value={newMakeMaterials.type}>
                            <option disabled></option>
                            <option className="" value="raw">Raw</option>
                            <option className="" value="semi-finished">Semi-finished</option>
                            <option className="" value="finished">Finished</option>
                        </select>
                    </div>
                    <div className="py-2 w-full flex space-x-2">
                        <label className="w-[13%]">Selling Price</label >
                        <input required onChange={handleChange} className=" w-9/12 bg-gray-50 focus:bg-slate-200" type="number" name="price" value={newMakeMaterials.price}></input>
                    </div>
                    <div className="py-2 w-full flex space-x-2">
                        <label className="w-[13%]">Measurement</label >
                        <input required onChange={handleChange} className=" w-9/12 bg-gray-50 focus:bg-slate-200" type="text" name="measurement" value={newMakeMaterials.measurement}></input>
                    </div>
                    <div className="py-2 w-full flex space-x-2">
                        <label className="w-[13%]">Initial Quantity</label >
                        <input required onChange={handleChange} className=" w-9/12 bg-gray-50 focus:bg-slate-200" type="number" name="quantity" value={newMakeMaterials.quantity}></input>
                    </div>
                    <button type="submit" className="float-right my-5 bg-white hover:bg-red-700 border-black hover:border-red-700 text-sm border-2 text-black py-1 px-7 rounded"><img src="/icon-save.png" alt="" width={30} height={30}/></button>
                </form>
            </div>
            
        </MainLayout>
        )
}