import { useContext, useEffect, useState } from "react";
import MainLayout from "../../components/mainLayout";
import SummaryBox from "../../components/summary/summaryBox";
import { checkResponseStatusOK, formatNumberCurrency } from "../../components/utilityFunc";
import { getExpenseOutcome } from "../../middleware/summary";
import { StatusMessageContext } from "../../stores/status-message-context";


export default function ExpenseOutcome() {
  const statusMessage = useContext(StatusMessageContext)

  const [expensesData, setExpenseData] = useState({
    HighestExpense: "-",
    SumExpense: 0,
    TotalExpense: 0,
  })
  const [isOpen, setIsOpen] = useState(false)
  const [currentPath, setCurrentPath] = useState(() => 'summary/expenseoutcome')

  useEffect(async () => {
    const [status, data] = await getExpenseOutcome()
    if (checkResponseStatusOK(status)) {
      setExpenseData(data)
    }
    statusMessage.showMessage(status, 'Fetch Expense Summary')
  }, [])

  return (
    <MainLayout isOpen={isOpen} setIsOpen={setIsOpen} currentPath={currentPath} setCurrentPath={setCurrentPath}>
      <div className="p-5 w-full">
        <h1 className="font-serif">Expenses Summary</h1>
        <div className="flex flex-row space-x-2">
          <SummaryBox title="Total Purchase Expense" boldText={formatNumberCurrency(expensesData.TotalExpense)} grayText="$"/>
          <SummaryBox title="Most Expensive Purchase" boldText={expensesData.HighestExpense}/>
          <SummaryBox title="Total All Expenses" boldText={formatNumberCurrency(expensesData.SumExpense)} grayText="$"/>
        </div>
      </div>
    </MainLayout>
  )
}