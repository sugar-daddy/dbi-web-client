import { useContext, useEffect, useState } from "react";
import MainLayout from "../../components/mainLayout";
import SummaryBox from "../../components/summary/summaryBox";
import { checkResponseStatusOK, formatNumberCurrency } from "../../components/utilityFunc";
import { getSalesIncome } from "../../middleware/summary";
import { StatusMessageContext } from "../../stores/status-message-context";


export default function SalesIncome() {
  const statusMessage = useContext(StatusMessageContext)

  const [salesData, setSalesData] = useState({
    BestSeller: "-",
    GrossSales: 0,
    NetSales: 0,
    TotalSales: 0,
  })
  const [isOpen, setIsOpen] = useState(false)

  useEffect(async () => {
    const [status, data] = await getSalesIncome()
    if (checkResponseStatusOK(status)) {
      setSalesData(data)
    }
    statusMessage.showMessage(status, 'Fetch Sales Summary')
  }, [])

  return (
    <MainLayout isOpen={isOpen} setIsOpen={setIsOpen}>
      <div className="p-5 w-full space-y-4">
        <div className="space-y-2">
          <h1 className="font-serif">Sales Summary</h1>
          <div className="flex flex-row space-x-2">
            <SummaryBox title="Total All Sales" boldText={formatNumberCurrency(salesData.TotalSales)} grayText="$"/>
            <SummaryBox title="Most Product Sold" boldText={salesData.BestSeller}/>
          </div>
        </div>
        <div className="space-y-2">
          <h1 className="font-serif">Income Summary</h1>
          <div className="flex flex-row space-x-2">
            <SummaryBox title="Net Sales" boldText={formatNumberCurrency(salesData.NetSales)} grayText="$"/>
            <SummaryBox title="Gross Sales" boldText={formatNumberCurrency(salesData.GrossSales)} grayText="$"/>
          </div>
        </div>
      </div>
    </MainLayout>
  )
}