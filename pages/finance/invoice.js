import Image from "next/image";
import { useContext, useEffect, useState } from "react";
import TableHeader from "/components/tableHeader"
import FilterGroup from "/components/filterGroup"
import MainLayout from "/components/mainLayout";
import Pagination from "../../components/pagination";
import PopUp from "../../components/popUp"
import AddInvoiceForm from "../../components/finance/invoiceForm";
import { UserContext } from "../../stores/user-context";
import { postInvoice } from "../../middleware/invoice";
import { checkResponseStatusOK, formatDatetimeToDate, formatNumberCurrency, sortArrayObjectByProp, sumPropValueOfArrayObject } from "../../components/utilityFunc";
import { StatusMessageContext } from "../../stores/status-message-context";
import { getSalesOrder } from "../../middleware/sales-order";
import { getPurchaseOrder } from "../../middleware/procurement";

export default function Invoice() {
  const userContext = useContext(UserContext)
  const statusMessage = useContext(StatusMessageContext)

  const [isOpen, setIsOpen] = useState(false)
  const [showAddPopUp, setShowAddPopUp] = useState(false)
  const [filter, setFilter] = useState('all')
  const [SOPOData, setSOPOData] = useState(null)
  const [pagedData, setPagedData] = useState(null)
  const [chosenData, setChosenData] = useState(null)

  const handleFetchData = async () => {
    let fetchedSO, fetchedPO = false
    let data = []
    if (filter == 'all' || filter == 'SO') {  // get SO
      const [status, dataSO] = await getSalesOrder({ is_accepted: 1 })
      if (checkResponseStatusOK(status)) {
        data = [
          ...data, 
          ...dataSO.map(({ id, date, invoice_exist, customer, salesdetails }) => ({
            id,
            date: formatDatetimeToDate(date),
            ref: 'SO',
            invoice_exist,
            external: customer,
            amount: sumPropValueOfArrayObject(salesdetails, 'price'),
          }))
        ]
        fetchedSO = true
      }
    }
    if (filter == 'all' || filter == 'PO') {  // get PO
      const [status, dataPO] = await getPurchaseOrder()
      if (checkResponseStatusOK(status)) {
        data = [
          ...data,
          ...dataPO.map(({ id, date, invoice_exist, vendor, purchasedetails }) => ({
            id,
            date: formatDatetimeToDate(date),
            ref: 'PO',
            invoice_exist,
            external: vendor,
            amount: sumPropValueOfArrayObject(purchasedetails, 'price'),
          }))
        ]
        fetchedPO = true
      }
    }
    if (fetchedSO || fetchedPO) {
      if (!fetchedPO && filter == 'all')
        statusMessage.showMessage(500, 'Fetch Purchase Order Data!')
      else if (!fetchedSO && filter == 'all')
        statusMessage.showMessage(500, 'Fetch Sales Order Data!')
      if (filter == 'all')
        sortArrayObjectByProp(data, 'date')
      setSOPOData(data)
    } else
      statusMessage.showMessage(500, 'Fetch Sales Order & Purchase Order Data!')
  }
  const handleAdd = (data) => {
    setShowAddPopUp(!showAddPopUp)
    setChosenData(data)
  }
  const handleSubmit = async e => {
    e.preventDefault()

    const invoice = {
      purchase_id: chosenData.id,
      sales_id: chosenData.id,
      employee_id: userContext.user.id,
    }
    // post invoice
    console.log(invoice)
    const status = await postInvoice(invoice, chosenData.ref)
    if (checkResponseStatusOK(status))
      statusMessage.showMessage(status, `Make Invoice of ${chosenData.ref} with ID ${chosenData.id}!`)
    else
      statusMessage.showMessage(status, 'Make Invoice!')

    await handleFetchData()
    setShowAddPopUp(false) // close pop up
  }

  useEffect(async () => {
    // get SO & PO data
    await handleFetchData()
  }, [filter])

  return (
    <MainLayout isOpen={isOpen} setIsOpen={setIsOpen}>
      <div className="p-5 space-y-2 w-full">
        <div className="flex justify-between relative">
          <div className="w-fit">
            <FilterGroup filters={['all', 'SO', 'PO']} activeFilter={filter} setActiveFilter={setFilter} />
          </div>
        </div>
        { SOPOData &&
        <>
          <TableHeader headingNames={['Date', 'SO/PO', 'ID', 'Customer/Vendor', 'Amount', 'Invoice Status']}>
            {pagedData && pagedData.map((data, idx) => 
              <tr key={idx} className="border-b-2 divide-x-2 w-full h-full">
                <td className="px-2 py-1 text-center">{data.date}</td>
                <td className="px-2 py-1 text-center">{data.ref}</td>
                <td className="px-2 py-1">{data.id}</td>
                <td className="px-2 py-1 capitalize">{data.external.name}</td>
                <td className={"px-2 py-1 text-right " + (data.ref == 'PO' ? "text-red-500" : "text-green-500")}>{formatNumberCurrency(data.amount)}</td>
                <td className="px-2 py-1 flex justify-center h-full">
                  {data.invoice_exist
                  ? <Image src="/assept-document.svg"  alt="" width={20} height={20}/>
                  : <button onClick={()=>handleAdd(data)}><Image src="/add-document.svg"  alt="" width={20} height={20}/></button>
                  }
                </td>
              </tr>
            )}
          </TableHeader>
          <Pagination data={SOPOData} setPagedData={setPagedData} itemsPerPage={5} />
        </>
        }
      </div>
      <PopUp showPopUp={showAddPopUp} setShowPopUp={setShowAddPopUp} title="Add Invoice">
        <AddInvoiceForm data={chosenData} handleSubmit={handleSubmit}/>
      </PopUp>
    </MainLayout>
  )
}