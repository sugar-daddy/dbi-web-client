import Image from "next/image";
import { useContext, useEffect, useState } from "react";
import TableHeader from "/components/tableHeader"
import FilterGroup from "/components/filterGroup"
import MainLayout from "/components/mainLayout";
import Pagination from "../../components/pagination";
import PopUp from "../../components/popUp"
import CashFlowForm from "../../components/finance/cashflowForm"
import { getCashFlow, postCashFlow, putCashFlow } from "../../middleware/cash-flow";
import { checkResponseStatusOK, formatDatetimeToDate, formatNumberCurrency } from "../../components/utilityFunc";
import { UserContext } from "../../stores/user-context";
import { StatusMessageContext } from "../../stores/status-message-context";

const emptyCashFlowData = {
  id: 0,
  date: '',
  status: '',
  descriptions: '',
  amount: 0,
}

export default function CashFlow() {
  const user = useContext(UserContext).user
  const statusMessage = useContext(StatusMessageContext)
  
  const [isOpen, setIsOpen] = useState(false)
  const [showAddPopUp, setShowAddPopUp] = useState(false)
  const [filter, setFilter] = useState('all')
  
  const [cashFlowData, setCashFlowData] = useState(null)
  const [pagedData, setPagedData] = useState(null)
  const [chosenData, setChosenData] = useState(emptyCashFlowData)
  
  const params = () => filter == 'all' ? {} : {'status': filter}
  const handleFetchData = async () => {
    const [status, filtered] = await getCashFlow(params())
    if (checkResponseStatusOK(status))
      setCashFlowData(filtered)
    else
      statusMessage.showMessage(status, 'Fetch Data!')
  }
  const handleChange = e => {
    const {name, value} = e.target
    if (chosenData?.id)
      setChosenData(data => ({
        ...data,
        [name]: value
      }))
    else
      setChosenData(data => ({
        ...data,
        date: new Date().toISOString().slice(0, 10),
        [name]: value
      }))
  }
  const handleSubmit= async e => {
    e.preventDefault()
    
    if (chosenData.id){
      //update data
      const status = await putCashFlow(chosenData)
      const text = `Update on Cash Report with ID ${chosenData.id}!`
      statusMessage.showMessage(status, text)
    } else {
      //add employee id
      const postData = Object.assign({}, chosenData, {
        employee_id:  user && user.id
      })
      //post data
      const status = await postCashFlow(postData)
      const text = 'Insert New Cash Report!'
      statusMessage.showMessage(status, text)
    }
    
    setChosenData(emptyCashFlowData)
    setShowAddPopUp(false) // close pop up
    // get cash flow data
    await handleFetchData()
  }

  useEffect(async () => {
    await handleFetchData()
  }, [filter])  // filter data by api request when filter changed
  
  useEffect(()=>{
    if(!showAddPopUp)
    setChosenData(emptyCashFlowData)
  }, [showAddPopUp])  // reset form when popup closed

  return (
    <>
    <MainLayout isOpen={isOpen} setIsOpen={setIsOpen}>
      <div className="p-5 space-y-2 w-full">
        <div className="flex justify-between relative">
          <div className="w-fit">
            <FilterGroup filters={['all', 'debit', 'credit']} activeFilter={filter} setActiveFilter={setFilter} />
          </div>
          <button onClick={()=>{setShowAddPopUp(!showAddPopUp); setChosenData(null)}} className="bg-gray-200 hover:bg-gray-300 border-2 border-gray-500 rounded-md py-0.5 px-1 h-fit absolute bottom-0 right-0 flex items-center"><Image src="/add.svg" alt="" width={20} height={20}/></button>
        </div>
        { cashFlowData && 
        <>
          <TableHeader headingNames={['Date', 'Status', 'Descriptions', 'Amount', 'Action']}>
            {pagedData && pagedData.map((data) => 
              <tr key={data.id} className="border-b-2 divide-x-2 w-full h-full">
                <td className="px-2 py-1 text-center">{formatDatetimeToDate(data.date)}</td>
                <td className="px-2 py-1 text-center capitalize">{data.status}</td>
                <td className="px-2 py-1">{data.descriptions}</td>
                <td className={"px-2 py-1 text-right " + (data.status == 'credit' ? "text-red-500" : "text-green-500")}>{formatNumberCurrency(data.amount)}</td>
                <td className="px-2 py-1 flex justify-center h-full">
                  <button onClick={()=>{setShowAddPopUp(!showAddPopUp); setChosenData(data)}}><Image src="/pencil.svg"  alt="" width={20} height={20}/></button>
                </td>
              </tr>
            )}
          </TableHeader>
          <Pagination data={cashFlowData} setPagedData={setPagedData} itemsPerPage={5} />
        </>
        }
      </div>
    </MainLayout>
    <PopUp showPopUp={showAddPopUp} setShowPopUp={setShowAddPopUp} title={`${chosenData?.id ? 'Edit' : 'Add'} Cash Flow`}>
      <CashFlowForm
        chosenData={chosenData}
        handleChange={handleChange}
        handleSubmit={handleSubmit}
      />
    </PopUp>
    </>
  )
}