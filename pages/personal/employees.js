import { useState } from "react";
import MainLayout from "../../components/mainLayout";
import ViewEmployees from "../../components/personal/viewemployees";
import AddNewEmployees from "../../components/personal/addnewemployees";

export default function Employees() {
  const [isOpen, setIsOpen] = useState(false)
  const [isAddNewEmployees, setAddNewEmployees] = useState(false)
  const [editData, setEditData] = useState(null)
  
  return (
    <MainLayout isOpen={isOpen} setIsOpen={setIsOpen}>
      {isAddNewEmployees
      ? <AddNewEmployees data={editData} setAddNewEmployees={setAddNewEmployees} />
      : <ViewEmployees setAddNewEmployees={setAddNewEmployees} setEditData={setEditData} />
      }
    </MainLayout>
  )
}