import Image from "next/image";
import { useContext, useEffect, useState } from "react";
import TableHeader from "/components/tableHeader"
import MainLayout from "/components/mainLayout";
import Pagination from "../../components/pagination";
import { getRequestLeave, putRequestLeave } from "../../middleware/request-leave";
import { checkResponseStatusOK, formatDatetimeToDate } from "../../components/utilityFunc";
import { StatusMessageContext } from "../../stores/status-message-context";

export default function LeaveRequests() {
  const [isOpen, setIsOpen] = useState(false)
  const [pagedData, setPagedData] = useState(null)
  const statusMessage = useContext(StatusMessageContext)
  const [leaveRequestData, setLeaveRequestData] =useState(null)

  const handleGetData = async() => {
    const [status,dataRequestLeave] = await getRequestLeave()
    if (checkResponseStatusOK(status))
      setLeaveRequestData(dataRequestLeave)
    else
      statusMessage.showMessage(status, 'Fetch Data!')
  }

  useEffect(async() =>{
    await handleGetData()
  },[]) 

  const handlerAction = async (idleaverequest, isAcceptIt) => {
    const data = {id: idleaverequest , is_accepted: isAcceptIt}
    const status = await putRequestLeave(data)
    const text = `Update on Leave Request with ID ${idleaverequest}!`
    statusMessage.showMessage(status, text)
    await handleGetData()
  }

  return (
    <MainLayout isOpen={isOpen} setIsOpen={setIsOpen} >
      <div className="p-5 space-y-2 w-full">
        <div>
          <span className="text-2xl">Accept Leave</span>
        </div>
        { leaveRequestData &&
          <>
            <TableHeader headingNames={['ID','Name', 'Leave Type', 'Start Date', 'End Date','Action']}>
              {pagedData && pagedData.map((leaveRequestData) => 
                <tr key={leaveRequestData.id} className="border-b-2 divide-x-2 w-full h-full">
                  <td className="px-2 py-1 text-center">{leaveRequestData.id}</td>
                  <td className="px-2 py-1 text-center capitalize">{leaveRequestData.person.name}</td>
                  <td className="px-2 py-1 text-center">{leaveRequestData.type}</td>
                  <td className="px-2 py-1 text-center">{formatDatetimeToDate(leaveRequestData.startdate)}</td>
                  <td className="px-2 py-1 text-center">{formatDatetimeToDate(leaveRequestData.enddate)}</td>
                  <td className="px-2 flex justify-around h-full">
                    {leaveRequestData.isaccepted == 0
                      ?
                      <>
                    <button className="hover:bg-rose-500 px-2 pt-1.5 rounded-md" onClick={()=>handlerAction(leaveRequestData.id, 2)}> <Image src="/cross.svg"  alt="" width={20} height={20}/> </button>
                    <button className="hover:bg-[#83BD75] px-2 pt-1.5 rounded-md" onClick={()=>handlerAction(leaveRequestData.id, 1)}><Image src="/check.svg"  alt="" width={20} height={20}/></button>
                      </>
                      : leaveRequestData.isaccepted == 1
                      ? <Image src="/check done.svg" alt="" width={20} height={32}/>
                      
                      : <Image src="/cross denied.svg" alt="" width={20} height={32}/>
                    }
                  </td>
                </tr>
              )}
            </TableHeader>
            <Pagination data={leaveRequestData} setPagedData={setPagedData} itemsPerPage={5}/>
          </>
        }
      </div>
    </MainLayout>
  )
}