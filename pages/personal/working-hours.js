import Image from "next/image";
import { useContext, useEffect, useState } from "react";
import TableHeader from "/components/tableHeader"
import MainLayout from "/components/mainLayout";
import Pagination from "../../components/pagination";
import { StatusMessageContext } from "../../stores/status-message-context";
import { checkResponseStatusOK, formatDatetimeToTime } from "../../components/utilityFunc";
import SetWorkingHours from "../../components/personal/setworkinghours";
import PopUp from "../../components/popUp";
import { getEmployee, putEmployee } from "../../middleware/employee";

export default function WorkingHours() {
  const [isOpen, setIsOpen] = useState(false)
  const [showSetWorkingHour, setShowSetWorkingHour] = useState(false)
  const [pagedData, setPagedData] = useState(null)
  const [workingHoursData, setWorkingHoursData] = useState(null)
  const statusMessage = useContext(StatusMessageContext)
  const [editData, setEditData] = useState({
    employeeid: 0,  
    monday_in: '', monday_out: '', 
    tuesday_in: '', tuesday_out: '', 
    wednesday_in: '', wednesday_out: '', 
    thursday_in: '', thursday_out: '', 
    friday_in: '', friday_out: '',
  })

  const handleGetData = async() => {
    const [status,dataWorkingHour] = await getEmployee()
    if (checkResponseStatusOK(status))
      setWorkingHoursData(dataWorkingHour)
    else
      statusMessage.showMessage(status, 'Fetch Data!')
  }

  const handleChange = e => {
    const {name, value} = e.target
    setEditData(data => ({
      ...data,
      [name]: value
    }))
  }

  const onClickButton = 
    ( employeeid, 
      monday_in,
      monday_out,
      tuesday_in,
      tuesday_out,
      wednesday_in,
      wednesday_out,
      thursday_in,
      thursday_out,
      friday_in,
      friday_out,
    ) =>{
        setEditData({
          employeeid: employeeid,  
          monday_in: formatDatetimeToTime(monday_in), monday_out: formatDatetimeToTime(monday_out), 
          tuesday_in: formatDatetimeToTime(tuesday_in), tuesday_out: formatDatetimeToTime(tuesday_out), 
          wednesday_in: formatDatetimeToTime(wednesday_in), wednesday_out: formatDatetimeToTime(wednesday_out), 
          thursday_in: formatDatetimeToTime(thursday_in), thursday_out: formatDatetimeToTime(thursday_out), 
          friday_in: formatDatetimeToTime(friday_in), friday_out: formatDatetimeToTime(friday_out),
        })
        console.log('test') 
        setShowSetWorkingHour(true)
  }

  const handleSubmit= async e => {
    e.preventDefault()

      //update data
      const status = await putEmployee(editData)
      const text = `Update on Working Hours with ID ${editData.employeeid}!`
      statusMessage.showMessage(status, text)
    
      setShowSetWorkingHour(false) // close pop up
    // get cash flow data
    await handleGetData()
  }

  useEffect(async() =>{
    await handleGetData()
  },[])  

  return (
    <>
    <MainLayout isOpen={isOpen} setIsOpen={setIsOpen}>
      <div className="p-5 space-y-2 w-full h-full text-lg">
        { workingHoursData &&
          <>
            <TableHeader  headingNames={['Part', 'Name', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Action']}>
              {pagedData && pagedData.map(workingHoursData => 
                <tr key={workingHoursData.id} className="border-b-2 divide-x-2 w-full h-full">
                  <td className="px-2 py-1 text-center capitalize"> {workingHoursData.part}</td>
                  <td className="px-2 py-1 text-center capitalize">{workingHoursData.name}</td>
                  <td className="px-2 py-1 text-center">{formatDatetimeToTime(workingHoursData.monday_in)+"-"+formatDatetimeToTime(workingHoursData.monday_out)}</td>
                  <td className="px-2 py-1 text-center">{formatDatetimeToTime(workingHoursData.tuesday_in)+"-"+formatDatetimeToTime(workingHoursData.tuesday_out)}</td>
                  <td className="px-2 py-1 text-center">{formatDatetimeToTime(workingHoursData.wednesday_in)+"-"+formatDatetimeToTime(workingHoursData.wednesday_out)}</td>
                  <td className="px-2 py-1 text-center">{formatDatetimeToTime(workingHoursData.thursday_in)+"-"+formatDatetimeToTime(workingHoursData.thursday_out)}</td>
                  <td className="px-2 py-1 text-center">{formatDatetimeToTime(workingHoursData.friday_in)+"-"+formatDatetimeToTime(workingHoursData.friday_out)}</td>
                  <td className="px-2 py-1 flex justify-center h-full">
                    <button type="button" onClick={()=>
                      onClickButton(workingHoursData.id, workingHoursData.monday_in, 
                        workingHoursData.monday_out, workingHoursData.tuesday_in, workingHoursData.thursday_out, workingHoursData.wednesday_in,
                        workingHoursData.wednesday_out, workingHoursData.thursday_in, workingHoursData.thursday_out, workingHoursData.friday_in, workingHoursData.friday_out
                      )}>
                        <Image src="/pencil.svg"  alt="" width={20} height={20}/>
                    </button>
                  </td>
                </tr>
              )}
            </TableHeader>
            <Pagination className="flex relative" data={workingHoursData} setPagedData={setPagedData} itemsPerPage={5}/>
          </>
        }
      </div>
    </MainLayout>
      <PopUp showPopUp={showSetWorkingHour} setShowPopUp={setShowSetWorkingHour} title={`set hours`}>
        <SetWorkingHours handlerchange={handleChange} 
        monday_in={editData.monday_in} tuesday_in={editData.tuesday_in}  wednesday_in={editData.wednesday_in}  
        thursday_in={editData.thursday_in} friday_in={editData.friday_in} 
        monday_out={editData.monday_out} tuesday_out={editData.tuesday_out}  wednesday_out={editData.wednesday_out}
        thursday_out={editData.thursday_out} friday_out={editData.friday_out}
        handlerSubmit={handleSubmit}/>
      </PopUp>
    </>
  )
}