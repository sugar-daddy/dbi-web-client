import {useContext, useState} from "react";
import { postAddVendor } from "../../middleware/vendor";
import { StatusMessageContext } from "../../stores/status-message-context";
import MainLayout from "/components/mainLayout";

export default function AddVendor() {
    const [isOpen, setIsOpen] = useState(false)
    const statusMessage = useContext(StatusMessageContext)

    const refreshData = {
        name: '',
        address: '',
        country: '',
        region: '',
        postalcode: '',
        city: '',
    }

    const handleSubmit= async e => {
        e.preventDefault()
        const status =  await postAddVendor(newAddVendor)
        const text = `Insert new Vendor !`
        statusMessage.showMessage(status, text)
        setNewAddVendor(refreshData)
    }

    const [newAddVendor, setNewAddVendor] = useState(refreshData)

    const handleChange = e => {
        const {name, value} = e.target
        setNewAddVendor(data => ({
          ...data,
          [name]: value
        }))
    }

    return(
        <MainLayout isOpen={isOpen} setIsOpen={setIsOpen}>
            <div className="p-5 space-y-2 w-full">
                <form onSubmit={handleSubmit} className="py-2 w-full text-lg">
                    <div className="py-2 w-full space-x-2 flex">
                        <label className=" w-[13%]">Vendor Name</label >
                        <input required onChange={handleChange} className=" w-10/12 bg-gray-50 focus:bg-slate-200" type="text" name="name" value={newAddVendor.name}></input>
                    </div>
                    <div className="py-2 w-full space-x-2 flex">
                        <label className="w-[13%]">Street/House Number</label >
                        <input required onChange={handleChange} className=" w-10/12 bg-gray-50 focus:bg-slate-200" type="text" name="address" value={newAddVendor.address}></input>
                    </div>
                    <div className="py-2 w-full space-x-2 flex">
                        <label className="w-[13%]">Country/Region</label >
                        <input required onChange={handleChange} className="w-5/12 bg-gray-50 focus:bg-slate-200" type="text" name="country" placeholder="Country" value={newAddVendor.country}></input>
                        <label  className="w-[1%]"></label >
                        <input required onChange={handleChange} className="w-[39.5%] bg-gray-50 focus:bg-slate-200" type="text" name="region" placeholder="Region" value={newAddVendor.region}></input>
                    </div>
                    <div className="py-2 w-full space-x-2 flex">
                        <label className="w-[13%]">Postal Code/City</label >
                        <input required onChange={handleChange} className="w-5/12 bg-gray-50 focus:bg-slate-200" type="text" name="postalcode" placeholder="Postal Code" value={newAddVendor.postalcode}></input>
                        <label className="w-[1%] "></label>
                        <input required onChange={handleChange} className="w-[39.5%] bg-gray-50 focus:bg-slate-200" type="text" name="city" placeholder="City" value={newAddVendor.city}></input>
                    </div>
                    <button className="my-5 bg-white hover:bg-red-700 border-black hover:border-red-700 text-xl border-4 text-black py-1 px-7 rounded">SAVE</button>
                </form>
            </div>
        </MainLayout>
    )
}