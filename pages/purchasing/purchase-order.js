import Image from "next/image";
import { useEffect, useState } from "react";
import TableHeader from "/components/tableHeader";
import MainLayout from "/components/mainLayout";
import { useContext } from "react";
import { UserContext } from "../../stores/user-context";
import { StatusMessageContext } from "../../stores/status-message-context";
import { getVendor } from "../../middleware/vendor";
import { getMaterial } from "../../middleware/material";
import { checkResponseStatusOK } from "../../components/utilityFunc";
import { postProcurement } from "../../middleware/procurement";

const emptyItem = {
  material_id: 0,
  rawName: '',
  quantity: 0,
  measurement:'',
  price: 0,
}
const emptyData = {
  id: 0,
  vendor_id: '',
  raws: [
    emptyItem
  ],
}

export default function PurchaseOrder() {
  const [isOpen, setIsOpen] = useState(false)
  const userContext = useContext(UserContext)
  const [vendorMember, setVendorMember] = useState(null)
  const [purchaseItem, setPurchaseItem] = useState(null)
  const statusMessage = useContext(StatusMessageContext)

  useEffect(async()=> {
    setNewData(emptyData)
    const [statusVendor,dataVendor] = await getVendor()
    const [statusPurchaseItem, dataPurchaseItem] = await getMaterial({type: 'raw'})
    if (checkResponseStatusOK(statusVendor))
      setVendorMember(dataVendor)
    else
      statusMessage.showMessage(statusVendor, 'Fetch Data!')

    if (checkResponseStatusOK(statusPurchaseItem)) {
      setPurchaseItem(dataPurchaseItem)
    } else {
      statusMessage.showMessage(statusPurchaseItem, 'Fetch Data!')
    }
  },[])

  const [newData, setNewData] = useState(emptyData)

  const handleSubmit= async e => {
    e.preventDefault()
    const data = {
      ...newData,
      employee_id: userContext.user.id
    }
    const status =  await postProcurement(data)
    const text = `Insert new Sales Order !`
    statusMessage.showMessage(status, text)
    // post/update data
    console.log(newData)
    setNewData(emptyData)
  }

const handleChange = e => {
  const {name, value} = e.target
  setNewData(data => ({
    ...data,
    [name]: value
  }))
}

const handleChangeItems = (e,idx) => {
  const {name, value} = e.target
  const updatedItem = Object.assign({}, newData.raws[idx], {[name]: value})
  if (name === 'rawName')
    updatedItem = Object.assign({}, updatedItem, {['material_id']: purchaseItem.find(item => item.name==value).id, ['measurement']: purchaseItem.find(item => item.name==value).measurement,['quantity']: 1})
  else if (updatedItem.material_id && name === 'quantity')
    if(value <= 0)
      updatedItem = Object.assign({}, updatedItem, {['quantity']: 1}) 

  setNewData(data=>({
    ...data,
    raws: [
      ...data.raws.slice(0, idx),
      updatedItem,
      ...data.raws.slice(idx + 1)
    ]
  }))
}

const handleAddItems = () => {
  setNewData(data => ({
    ...data,
    raws: [...data.raws, emptyItem]
  }))
}

const handleRemoveItems = idx => {
  setNewData(data => ({
    ...data,
    raws: data.raws.filter((_, i) => i != idx)
  }))
}


  const [totalHarga, setTotalHarga] = useState(0)

  useEffect(()=>{
    let harga = 0
    newData.raws.map((raw)=>
      {
        harga = parseInt(raw.price) * raw.quantity + harga
      }
    )
    setTotalHarga(harga)
  }, [
    newData.raws
  ]) 

  return (
    <MainLayout isOpen={isOpen} setIsOpen={setIsOpen}>
      <div className="p-5 space-y-2 w-full">
        <div className="space-x-2">
          <label>Vendor :</label>
          <select name="vendor_id" value={newData.vendor_id} onChange={handleChange} required className="capitalize">
            <option disabled></option>
            {vendorMember &&
              vendorMember.map(data => 
                <option key={data.id} value={data.id}> {data.name} </option>
            )}
          </select>
        </div>
        <div className="ml-5 pt-3">
          <h2>Total Price <span className="border-black	border-2 py-1 px-3 ml-5 rounded" >{totalHarga} </span></h2>
        </div>
        <form onSubmit={handleSubmit}>
          <TableHeader headingNames={['Kode Barang','Nama Barang', 'Quantity', 'Unit', '@ Price', 'Action']}>
            {newData && newData.raws
            .map((data,idx) =>
              <tr key={idx} className="border-b-2 divide-x-2 w-full h-full">
                <td className="px-2 py-1 text-center">
                  <input type="text" name="material_id" value={data.material_id} className="w-full text-center border-0" disabled required/>
                </td>
                <td className="px-2 py-1 text-center">
                  <select type="text" name="rawName" value={data.rawName} onChange={e=>handleChangeItems(e,idx)} className="w-full text-center border-0 capitalize" required>
                    <option disabled></option>
                    {purchaseItem &&
                      purchaseItem.map(material =>
                        <option key={material.id} value={material.name}>{material.name}</option>
                    )}
                  </select>
                </td>
                <td className="px-2 py-1 text-center">
                  <input type="number" min={1} name="quantity" value={data.quantity} className="w-full text-center border-0" onChange={e=>handleChangeItems(e,idx)} required />
                </td>
                <td className="px-2 py-1 text-center">
                  <input type="text" name="measurement" value={data.measurement} className="w-full text-center border-0" disabled required/>
                </td>
                <td className="px-2 py-1 text-center">
                  <input type="number" name="price" value={data.price} className="w-full text-center border-0" onChange={e=>handleChangeItems(e,idx)} required />
                </td>
                <td className="px-2 py-1 flex justify-center space-x-8 h-full">
                  <button type="button" className="rounded-md px-2 hover:bg-gray-200" onClick={()=>handleRemoveItems(idx)}><Image src="/icon-trash.png" alt="" width={20} height={20} /></button>
                </td>
              </tr>
            )}
          </TableHeader>
          <div className="flex flex-row-reverse">
            <button type="button" onClick={handleAddItems}><Image src="/add.svg" alt="" width={30} height={30}/></button>
          </div>          
          <button className="my-5 bg-white hover:bg-red-700 border-black hover:border-red-700 text-xl border-2 text-black py-1 px-2 rounded"  type="submit">POST</button>
        </form>
      </div>
    </MainLayout>
  )
}