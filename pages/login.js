import Image from "next/image";
import { useRouter } from "next/router";
import { useContext, useEffect, useState } from "react";
import { fetchCookie, fetchLogin } from "../middleware/authentication";
import { UserContext } from "../stores/user-context";

export default function Login() {
    const router = useRouter()
    const userContext = useContext(UserContext)
    
    const [username, setUsername] = useState('')
    const [password, setPassword] = useState('')
    const [isLoginStatus, setIsLoginStatus] = useState(0)

    const handleLogin = async event => {
        event.preventDefault()
        // login
        const [status, data] = await fetchLogin(username, password)
        if (status >= 200 && status < 300) {
            userContext.setUser(data)
            setIsLoginStatus(1)
        } else {
            setIsLoginStatus(-1)
        }
    }

    useEffect( () => {
        if (isLoginStatus == 1)
            router.push('/')
        else if (isLoginStatus == -1) {
            setUsername('')
            setPassword('')
            setTimeout(() => setIsLoginStatus(0), 2000)
        }
    }, [isLoginStatus])

    useEffect(async () => {
        // get server cookie
        const [status, data] = await fetchCookie()
        // if exist, setIsLoginStatus(1)
        if (status >= 200 && status < 300) {
            userContext.setUser(data)
            setIsLoginStatus(1)
        }
    }, [])

    return (
        <div className="bg-red-800 h-screen pt-6">
            <div className="flex flex-col justify-center items-center space-y-2">
                <Image src="/logo.png" alt="DBI logo" width="360" height="225" className="w-80" />
                <p className={`${isLoginStatus == -1 ? '' : 'invisible'} text-white font-semibold tracking-widest text-lg bg-red-600 py-1 px-2 border-2 border-white rounded-xl`}>
                    Login Failed!
                </p>
                <form className="w-64" onSubmit={handleLogin}>
                    <div className="mb-3">
                        <label htmlFor="username" className="block mb-2 text-sm font-medium text-gray-300">Username</label>
                        <input type="text" id="username" name="username" value={username} onChange={(e)=>setUsername(e.target.value)} className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5" required></input>
                    </div>
                    <div className="mb-6">
                        <label htmlFor="password" className="block mb-2 text-sm font-medium text-gray-300">Password</label>
                        <input type="password" id="password" name="password" value={password} onChange={(e)=>setPassword(e.target.value)} className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5" required></input>            
                    </div>
                    <div className="flex flex-col justify-center items-center">
                        <button type="submit" className="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center">Login</button>
                    </div>
                </form>
            </div>
        </div>
    )
}


