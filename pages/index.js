import { useState } from 'react';
import MainLayout from '../components/mainLayout';

export default function Home() {
  const [isOpen, setIsOpen] = useState(false)

  return (
    <>
      <MainLayout isOpen={isOpen} setIsOpen={setIsOpen}>
        <div className="w-full h-[calc(100vh-5rem)] flex items-center justify-center">
          <div className="font-serif font-bold italic flex flex-col items-end">
            <h1 className="tracking-wider text-4xl">&ldquo;Hari yang Bahagia&rdquo;</h1>
            <p>- Andreas Jumaga</p>
          </div>
        </div>
      </MainLayout>
    </>
  );
}
