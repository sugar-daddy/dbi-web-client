import {useContext, useState} from "react";
import { postMakeCustomer } from "../../middleware/customer";
import { StatusMessageContext } from "../../stores/status-message-context";
import MainLayout from "/components/mainLayout";

export default function AddCustomer() {
    const [isOpen, setIsOpen] = useState(false)
    const statusMessage = useContext(StatusMessageContext)
    const refreshData = {
        name: '',
        address: '',
        country: '',
        region: '',
        postal_code: '',
        city: '',
    }

    const handleSubmit= async e => {
        e.preventDefault()
        const status =  await postMakeCustomer(newAddCustomer)
        const text = `Insert new Customer !`
        statusMessage.showMessage(status, text)
        setNewAddCustomer(refreshData)
    }

    const [newAddCustomer, setNewAddCustomer] = useState(refreshData)

    const handleChange = e => {
        const {name, value} = e.target
        setNewAddCustomer(data => ({
          ...data,
          [name]: value
        }))
    }

    return(
        <MainLayout isOpen={isOpen} setIsOpen={setIsOpen} >
            <div className="p-5 space-y-2 w-full ">
                <form onSubmit={handleSubmit} className="py-2 w-full text-lg">
                    <div className="py-2 w-full flex space-x-2">
                        <p className="w-[13%]">Customer Name</p >
                        <input required onChange={handleChange} className=" w-10/12 bg-gray-50 focus:bg-slate-200 " type="text" name="name" value={newAddCustomer.name}></input>
                    </div>
                    <div className="py-2 w-full flex space-x-2">
                        <p className="w-[13%]">Street/House Number</p >
                        <input required onChange={handleChange} className="w-10/12 bg-gray-50 focus:bg-slate-200" type="text" name="address" value={newAddCustomer.address} ></input>
                    </div>
                    <div className="py-2 w-full flex space-x-2">
                        <p className="w-[13%]">Country/Region</p >
                        <input required onChange={handleChange} className=" w-5/12 bg-gray-50 focus:bg-slate-200" type="text" name="country" value={newAddCustomer.country} placeholder="Country"></input>
                        <p  className="w-[1%]"></p >
                        <input required onChange={handleChange} className="w-[39.5%] bg-gray-50 focus:bg-slate-200" type="text" name="region" value={newAddCustomer.region} placeholder="Region"></input>
                    </div>
                    <div className="py-2 w-full flex space-x-2">
                        <p className="w-[13%]">Postal Code/City</p >
                        <input required onChange={handleChange} className="w-5/12 bg-gray-50 focus:bg-slate-200" type="text" name="postal_code" value={newAddCustomer.postal_code} placeholder="Postal Code"></input>
                        <p  className="w-[1%]"></p >
                        <input required onChange={handleChange} className="w-[39.5%] bg-gray-50 focus:bg-slate-200" type="text" name="city" value={newAddCustomer.city} placeholder="City"></input>
                    </div>
                    <button className="my-5 bg-white hover:bg-red-700 border-black hover:border-red-700 text-xl border-4 text-black py-1 px-7 rounded">SAVE</button>
                </form>
            </div>  
        </MainLayout>
        )
}