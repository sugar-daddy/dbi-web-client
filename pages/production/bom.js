import { useState } from "react"
import MainLayout from "../../components/mainLayout"
import NewEditBOM from "../../components/production/new-edit-bom"
import ViewBOM from "../../components/production/view-bom"

export default function BOM() {
  const [isOpen, setIsOpen] = useState(false)
  const [isNewEdit, setIsNewEdit] = useState(null)
  const [editData, setEditData] = useState(null)
  const [bomData, setBomData] = useState(null)

  return (
    <MainLayout isOpen={isOpen} setIsOpen={setIsOpen}>
      {isNewEdit
      ? <NewEditBOM data={editData} isNewEdit={isNewEdit} setIsNewEdit={setIsNewEdit} bomData={bomData} />
      : <ViewBOM setIsNewEdit={setIsNewEdit} setEditData={setEditData} setBomData={setBomData} />
      }
    </MainLayout>
  )
}