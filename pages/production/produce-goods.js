import { useContext, useEffect, useState } from "react"
import MainLayout from "../../components/mainLayout"
import Pagination from "../../components/pagination"
import PopUp from "../../components/popUp"
import ProduceGoodsForm from "../../components/production/produce-goods-form"
import SearchInput from "../../components/searchInput"
import TableHeader from "../../components/tableHeader"
import { checkResponseStatusOK, sortArrayObjectByProp } from "../../components/utilityFunc"
import { getBom } from "../../middleware/bom"
import { StatusMessageContext } from "../../stores/status-message-context"
import { UserContext } from "../../stores/user-context"
import { postProduceGoods } from "../../middleware/produce-goods";

export default function ProduceGoods() {
  const userContext = useContext(UserContext)
  const statusMessage = useContext(StatusMessageContext)

  const [allProducts, setAllProducts] = useState(null)
  const [isOpen, setIsOpen] = useState(false)
  const [showPopUp, setShowPopUp] = useState(false)
  const [pagedData, setPagedData] = useState(null)
  const [filteredData, setFilteredData] = useState(null) // for search
  const [chosenData, setChosenData] = useState(null)

  const handleSubmit = async e => {
    e.preventDefault()
    // post data
    const status = await postProduceGoods(chosenData)
    statusMessage.showMessage(status, `Request to Produce Goods with ID ${chosenData.materialId}`)
    setShowPopUp(false)
  }
  const handleChange = e => {
    const {name, value} = e.target
    setChosenData(data => ({
      ...data,
      [name]: value
    }))
  }
  const handleProduce = product => {
    setChosenData({
      materialId: product.id,
      materialName: product.material_finished.name,
      quantity: '',
      measurement: product.material_finished.measurement,
      produceDate: '',
      employee_id: userContext.user.id,
    })
    setShowPopUp(!showPopUp)
  }
  const handleSearch = input => {
    if (input && +input === +input)
      setFilteredData(allProducts.filter(product => product.id == input))
    else if (input && typeof input === 'string')
      setFilteredData(allProducts.filter(product => product.material_finished.name.toLowerCase().includes(input)))
    else
      setFilteredData(allProducts)
  }

  useEffect(async () => {
    const [status, data] = await getBom()
    if (checkResponseStatusOK(status)) {
      const sorted = sortArrayObjectByProp(data, 'id')
      setAllProducts(sorted)
      setFilteredData(sorted)
    } else
      statusMessage.showMessage(status, 'Fetch Data!')
  }, [])

  return (
    <MainLayout isOpen={isOpen} setIsOpen={setIsOpen}>
      <div className="p-5 w-full space-y-8">
        <SearchInput searchFunction={handleSearch} label="Materials" placeholder="Search ID/Name..." />
        <div className="space-y-2">
          {filteredData &&
          <>
            <TableHeader headingNames={['ID', 'Name', 'Action']}>
              {pagedData && pagedData.map(product =>
                <tr key={product.id} className="border-b-2 divide-x-2 w-full h-full">
                  <td className="px-2 py-1 text-center">{product.id}</td>
                  <td className="px-2 py-1 text-center capitalize">{product.material_finished.name}</td>
                  <td className="px-2 py-1 flex justify-center space-x-8 h-full">
                    <button onClick={()=>handleProduce(product)} className="px-2 py-px border-2 rounded-md hover:bg-gray-200">Produce</button>
                  </td>
                </tr>
              )}
            </TableHeader>
            <Pagination data={filteredData} setPagedData={setPagedData} itemsPerPage={5} />
          </>
          }
        </div>
      </div>
      <PopUp showPopUp={showPopUp} setShowPopUp={setShowPopUp} title="Produce Goods">
          <ProduceGoodsForm produceData={chosenData} handleChange={handleChange} handleSubmit={handleSubmit} />
      </PopUp>
    </MainLayout>
  )
}